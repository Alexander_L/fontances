package com.example.usser.fontances;

import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.usser.fontances.data.DBWorker;

import java.util.UUID;

import sun.bob.mcalendarview.vo.DateData;

public class UserCreatingActivity extends Activity {

    private DBWorker dbWorker;
    User creatingUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_creating);

        //dbWorker = new DBWorker(this);
    }

    public void onBtnSaveUserInBaseClick(View view) {

        creatingUser = new User(getUserName(), getUserSurname(), getUserUUID(), getUserBirthDate(), getPathToAvatar(), true);
        if (isAllFieldsNotNull()) {
            DBWorker.getInstance(getApplicationContext()).addUserInBase(creatingUser);
            DBWorker.getInstance(getApplicationContext()).addLoginInBase(getUserLogin(), getUserPassword(), creatingUser.getUserUUID()); //TODO Must be checked for unique user
            Toast.makeText(this, R.string.ok, Toast.LENGTH_SHORT).show();
        }
    }


    private String getUserName () {
        EditText userNameEditText = findViewById(R.id.edTxt_input_user_name);
        return userNameEditText.getText().toString();
    }

    private String getUserSurname() {
        EditText userSurnameEditText = findViewById(R.id.edTxt_input_user_surename);
        return userSurnameEditText.getText().toString();
    }

    private DateData getUserBirthDate(){
        EditText userBirthday = findViewById(R.id.edTxt_input_user_birthDate);
        String notSplittedDataString = userBirthday.getText().toString();
        String [] userBirthdayByParts = notSplittedDataString.split("-");
        int m = userBirthdayByParts.length;
        return new DateData (Integer.parseInt(userBirthdayByParts[2]), Integer.parseInt(userBirthdayByParts[1]), Integer.parseInt(userBirthdayByParts[0])); //YY-MM-DD
    }

    private UUID getUserUUID(){
        return UUID.randomUUID();
    }

    private String getPathToAvatar(){
        ImageView avatar = findViewById(R.id.imageview_user_avatar); //TODO make here work element
        String pathToAvatar = "test_path_to_avatar";
        return pathToAvatar;
    }

    private String getUserLogin(){
        EditText userLogin = findViewById(R.id.edTxt_input_user_login);
        return userLogin.getText().toString();
    }

    private String getUserPassword(){
        EditText userPassword = findViewById(R.id.edTxt_input_user_password);
        return userPassword.getText().toString();
    }

    private boolean isAllFieldsNotNull(){
        boolean allFieldsNotNull = true;
        if(creatingUser.getName().length() == 0){
            allFieldsNotNull = false;
            Toast.makeText(this, R.string.fill_user_name_field, Toast.LENGTH_SHORT).show();
        }
        else if (creatingUser.getSurname().length() == 0){
            allFieldsNotNull = false;
            Toast.makeText(this, R.string.fill_user_surname_field, Toast.LENGTH_SHORT).show();
        }
        else if (creatingUser.getBirthday().getYear() == 0 || creatingUser.getBirthday().getMonth() == 0 || creatingUser.getBirthday().getDay() == 0){
            allFieldsNotNull = false;
            Toast.makeText(this, R.string.fill_user_birthday_field, Toast.LENGTH_SHORT).show();
        }
        else if(getUserLogin().length() == 0){
            allFieldsNotNull = false;
            Toast.makeText(this, R.string.fill_user_login_field, Toast.LENGTH_SHORT).show();
        }
        else if (getUserPassword().length() == 0){
            allFieldsNotNull = false;
            Toast.makeText(this, R.string.fill_user_password_field, Toast.LENGTH_SHORT).show();
        }

        return allFieldsNotNull;
    }


    public void onBtnDestroyUsersClick(View view) {
     DBWorker.getInstance(getApplicationContext()).destoryAllUsersRecords();
    }
}
