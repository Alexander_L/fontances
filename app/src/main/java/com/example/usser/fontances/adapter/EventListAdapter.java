package com.example.usser.fontances.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.usser.fontances.Event;
import com.example.usser.fontances.R;
import com.example.usser.fontances.data.EventStatus;

import java.util.ArrayList;

public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.EventViewHolder> {
    @NonNull
    @Override
    public EventViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.event_card, viewGroup, false);
        EventViewHolder eventViewHolder = new EventViewHolder(view);

        return eventViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull EventViewHolder eventViewHolder, int i) {
        //TODO Check here for correct using sorting
        eventViewHolder.eventName.setText(eventArrayList.get(i).getEventName());
        if(eventArrayList.get(i).getEventStatusEnum().equals(EventStatus.CREATE)){
                eventViewHolder.statusView.setImageResource(R.drawable.ic_status_created_48dp);
        }
        else if(eventArrayList.get(i).getEventStatusEnum().equals(EventStatus.UNDER_CONSIDERATION)){
            eventViewHolder.statusView.setImageResource(R.drawable.ic_status_under_consideration_48dp);
        }
        else if(eventArrayList.get(i).getEventStatusEnum().equals(EventStatus.APPROVING)){
            eventViewHolder.statusView.setImageResource(R.drawable.ic_status_approving_48_dp);
        }
        else if(eventArrayList.get(i).getEventStatusEnum().equals(EventStatus.ACCEPTED)){
            eventViewHolder.statusView.setImageResource(R.drawable.ic_status_accepted_48dp);
        }
        else if(eventArrayList.get(i).getEventStatusEnum().equals(EventStatus.REJECTED_BY_SYSTEM)){
            eventViewHolder.statusView.setImageResource(R.drawable.ic_status_rejected_48_dp);
        }

        //String dateText = eventArrayList.get(i).getSuggestedDate().getDayString() + " " + eventArrayList.get(i).getSuggestedDate().getMonthString();
        //eventViewHolder.eventDate.setText(eventArrayList.get(i).getSuggestedDate().getDayString());

    }

    public Event getItem(int position) {
        return eventArrayList.get(position);
    }

    @Override
    public int getItemCount() {
        return eventArrayList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    public static class EventViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        ImageView statusView;
        TextView eventName;
        TextView eventDate;

        EventViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.event_cardview);
            statusView = (ImageView) itemView.findViewById(R.id.view_card_draw_status);
            eventName = (TextView) itemView.findViewById(R.id.tv_card_event_name);
            eventDate = (TextView) itemView.findViewById(R.id.tv_card_event_date);
        }

    }

    ArrayList<Event> eventArrayList; //Need look and know how it works. This is adapter constructor.

    public EventListAdapter(ArrayList<Event> eventArrayList) {
        this.eventArrayList = eventArrayList;
    }

}
