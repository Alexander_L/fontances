package com.example.usser.fontances;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import com.example.usser.fontances.data.EventOccasion;
import com.example.usser.fontances.data.EventStatus;

import sun.bob.mcalendarview.vo.DateData;

public class Event implements Parcelable {


    private String eventName;
    private UUID eventUUID;
    private Enum<EventOccasion> eventOccasionEnum;
    private Enum<EventStatus> eventStatusEnum;
    private ArrayList<DateData> suggestedDates;
    private ArrayList<DateData> confirmedDates;

    private DateData acceptedDate;

    private UUID eventInitiatorUUID;
    private UUID eventEditorUUID;
    private HashMap<UUID, ArrayList<DateData>> hashMapConfirmedDates;
    public Event(String eventName,
                 UUID event_UUID,
                 Enum<EventOccasion> eventOccasionEnum,
                 Enum<EventStatus> eventStatusEnum,
                 ArrayList<DateData> suggestedDates,
                 ArrayList<DateData> confirmedDates,
                 DateData acceptedDate,
                 UUID eventInitiatorUUID,
                 UUID eventEditorUUID,
                 HashMap<UUID, ArrayList<DateData>> hashMapConfirmedDates) {
        this.eventName = eventName;
        this.eventUUID = event_UUID;
        this.eventOccasionEnum = eventOccasionEnum;
        this.eventStatusEnum = eventStatusEnum;
        this.suggestedDates = suggestedDates;
        this.confirmedDates = confirmedDates;
        this.acceptedDate = acceptedDate;
        this.eventInitiatorUUID = eventInitiatorUUID;
        this.eventEditorUUID = eventEditorUUID;
        this.hashMapConfirmedDates = hashMapConfirmedDates;

    }

    public void setSuggestedDates(ArrayList<DateData> suggestedDates) {
        this.suggestedDates = suggestedDates;
    }

    public static final Creator<Event> CREATOR = new Creator<Event>() {
        @Override
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        /*dest.writeString(getEventName());
        dest.writeString(getEventUUID().toString());
        dest.writeString(getEventOccasionEnum().toString());
        dest.writeString(getEventStatusEnum().toString());
        dest.writeArray(getSuggestedDates().toArray());
        dest.writeArray(getConfirmedDates().toArray());
        dest.writeString(getAcceptedDate().toString());
        dest.writeString(getEventInitiatorUUID().toString());
        dest.writeString(getEventEditorUUID().toString());
        dest.writeMap(getHashMapConfirmedDates());*/
        dest.writeValue(this); //TODO check here for transmit Event instance, not fields collection
    }

    public void setAcceptedDate(DateData acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    public Event getEventInstance() {
        return this;
    }

    public String getEventName() {
        return eventName;
    }

    public UUID getEventUUID() {
        return eventUUID;
    }

    public Enum<EventOccasion> getEventOccasionEnum() {
        return eventOccasionEnum;
    }

    public Enum<EventStatus> getEventStatusEnum() {
        return eventStatusEnum;
    }

    public ArrayList<DateData> getSuggestedDates() {
        return suggestedDates;
    }

    public ArrayList<DateData> getConfirmedDates() {
        return confirmedDates;
    }

    public DateData getAcceptedDate() {
        return acceptedDate;
    }

    public UUID getEventInitiatorUUID() {
        return eventInitiatorUUID;
    }

    public UUID getEventEditorUUID() {
        return eventEditorUUID;
    }

    public HashMap<UUID, ArrayList<DateData>> getHashMapConfirmedDates() {
        return hashMapConfirmedDates;
    }

    public boolean hasClearHashMapConfirmedDates () {
        hashMapConfirmedDates.clear();
        return hashMapConfirmedDates.isEmpty();
    }

    public void setEventStatusEnum(Enum<EventStatus> eventStatusEnum) {
        this.eventStatusEnum = eventStatusEnum;
    }

    public void setHashMapConfirmedDatesElement(UUID editorUUID, ArrayList<DateData> confirmedDates) {
        hashMapConfirmedDates.put(editorUUID, confirmedDates);
    }


    protected Event(Parcel in) {
        in.readValue(Event.class.getClassLoader());
    }

}