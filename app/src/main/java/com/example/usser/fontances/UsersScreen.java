package com.example.usser.fontances;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.usser.fontances.adapter.EventListAdapter;
import com.example.usser.fontances.adapter.UsersListAdapter;
import com.example.usser.fontances.data.DBWorker;
import com.example.usser.fontances.testpack.TestDBData;
import com.example.usser.fontances.listner.RecyclerClickListener;

import java.util.ArrayList;

public class UsersScreen extends AppCompatActivity {
    RecyclerView recyclerViewUsersScreen;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.users_screen);
        recyclerViewUsersScreen = findViewById(R.id.recyclerView_users_screen);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewUsersScreen.setLayoutManager(linearLayoutManager);
        //final UsersListAdapter usersListAdapter = new UsersListAdapter(TestDBData.getInstance().getUsersList());
        final UsersListAdapter usersListAdapter = new UsersListAdapter(DBWorker.getInstance(getApplicationContext()).getAllUsersArrayListFromBase());
        recyclerViewUsersScreen.setAdapter(usersListAdapter);
        recyclerViewUsersScreen.setHasFixedSize(true);

        recyclerViewUsersScreen.addOnItemTouchListener(new RecyclerClickListener(this) {
            @Override
            public void onItemClick(RecyclerView recyclerView, View itemView, int position) {
                Toast.makeText(UsersScreen.this, usersListAdapter.getItem(position).getName(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean b) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar_users_screen, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if(id == R.id.appBar_usersMenu_events_ico){
            Intent intent = new Intent(UsersScreen.this, EventsScreen.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
