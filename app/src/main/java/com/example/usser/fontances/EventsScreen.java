package com.example.usser.fontances;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.usser.fontances.adapter.EventListAdapter;
import com.example.usser.fontances.data.DBWorker;
import com.example.usser.fontances.data.EventStatus;
import com.example.usser.fontances.testpack.TestDBData;
import com.example.usser.fontances.listner.RecyclerClickListener;
import com.example.usser.fontances.util.EventCompiler;

import sun.bob.mcalendarview.vo.DateData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class EventsScreen extends AppCompatActivity {
    RecyclerView recyclerViewEventsScreen;
    EventListAdapter eventListAdapter;
    static EventsScreen instance;
    private String eventNameToTransmit;
    private AlertDialog.Builder alertDialogBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.events_screen);
        instance = this;
        eventNameToTransmit = "transmittingEvent";
        //DBWorker dbWorker = new DBWorker(this); //TODO Check here how correct start here DBWorker instance? Upd. DBWorker starts in Login activity.
        recyclerViewEventsScreen = findViewById(R.id.recyclerView_events_screen);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewEventsScreen.setLayoutManager(linearLayoutManager);
        //eventListAdapter = new EventListAdapter(TestDBData.getInstance().getEventsArrayList());
        eventListAdapter = new EventListAdapter(EventCompiler.getInstance(getApplicationContext()).getCompiledEventsArrayList());
        recyclerViewEventsScreen.setAdapter(eventListAdapter);
        recyclerViewEventsScreen.setHasFixedSize(true);
        recyclerViewEventsScreen.addOnItemTouchListener(new RecyclerClickListener(this) {
            @Override
            public void onItemClick(RecyclerView recyclerView, View itemView, int position) {
                Toast.makeText(EventsScreen.this, eventListAdapter.getItem(position).getEventName(), Toast.LENGTH_SHORT).show();
                Event selectedEvent = eventListAdapter.getItem(position);
               /* String returnedClass = getClassForActivityByConditions(selectedEvent);
                Intent intent = new Intent(EventsScreen.this, .class);
*/
                getClassForActivityByConditions(selectedEvent);
                //TODO Here i want take temporary class name for insert in Intent calling

                    /* Intent intent = new Intent(EventsScreen.this, UsersScreen.class);
                startActivity(intent);*/

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean b) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.app_bar_events_screen, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.appBar_eventsMenu_users_ico) {
            Intent intent = new Intent(EventsScreen.this, UsersScreen.class);
            startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }

    public void notifyAdapterDataSetChanged() { //From instance not cool?
        eventListAdapter.notifyDataSetChanged();

    }

    public static EventsScreen getInstance() {
        return instance;
    }

    public String getEventNameToTransmit() {
        return eventNameToTransmit;
    }

    protected void onFABCreateClick(View view) {
        Intent intent = new Intent(EventsScreen.this, EventCreatingScreenActivity.class);
        //Intent intent = new Intent(EventsScreen.this, UsersScreen.class);
        startActivity(intent);
    }

    private void reactForRejectByInitiator(Event rejectedEvent) {
            String userNameAndSurname = DBWorker.getInstance(getApplicationContext()).getUserNameSurnameInStringByUuid(rejectedEvent.getEventEditorUUID());
            eventRejectingByUserReaction(userNameAndSurname, rejectedEvent);

/*        else if(rejectedEvent.getEventStatusEnum().equals(EventStatus.REJECTED_BY_SYSTEM)){

        }
        eventRejectingBySystemReaction(rejectedEvent);*/
    }

    private void eventRejectingByUserReaction(String rejectedUserNameSurname, final Event rejectingEvent) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(R.string.event_rejected_by)
                .append(rejectedUserNameSurname)
                .append(R.string.continue_without_or_reject);
        alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setTitle(R.string.warning);
        alertDialogBuilder.setMessage(stringBuilder.toString());
        alertDialogBuilder.setNegativeButton(R.string.reject, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int which) {
                rejectEventFinally(rejectingEvent);
                dialogInterface.dismiss();
            }
        });
        alertDialogBuilder.setPositiveButton(R.string.continue_without, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                removeRejectingUserFromEventMemberList(rejectingEvent);
                dialogInterface.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    private void rejectEventFinally(Event rejectingEvent) {
        EventMembersList rejectedEventMembersList = DBWorker.getInstance(getApplicationContext()).getCertainEventMembersList(rejectingEvent.getEventUUID());
        rejectedEventMembersList.removeRecordFromList(Login.getInstance().currentUser.getUserUUID());
        DBWorker.getInstance(getApplicationContext()).updateEventMembersListInDatabase(rejectedEventMembersList);
    }

    private void removeRejectingUserFromEventMemberList(Event rejectingEvent) {
        EventMembersList rejectedEventMembersList = DBWorker.getInstance(getApplicationContext()).getCertainEventMembersList(rejectingEvent.getEventUUID());
        rejectedEventMembersList.removeRecordFromList(rejectingEvent.getEventEditorUUID());
        DBWorker.getInstance(getApplicationContext()).updateEventMembersListInDatabase(rejectedEventMembersList);

    }

    /* Wrong idea.
    private void eventRejectingBySystemReaction(final Event rejectingEvent){
        alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setTitle(R.string.warning);
        alertDialogBuilder.setMessage(R.string.event_rejected_by_system_message);
        alertDialogBuilder.setNegativeButton(R.string.reject, new DialogInterface.OnClickListener(){
            public void onClick(DialogInterface dialogInterface, int which){
                rejectEventFinally(rejectingEvent);
                dialogInterface.dismiss();
            }
        });
        alertDialogBuilder.setPositiveButton(R.string.enter_new_dates, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                Intent intent = new Intent(EventsScreen.this, DatesSelectionActivity.class);
                intent.putExtra(eventNameToTransmit, rejectingEvent);
                startActivity(intent);
                dialogInterface.dismiss();

            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }*/


    private void getClassForActivityByConditions(Event selectedEvent) { //TODO Ugly, hard method for UI class. Need refactor.
        Enum selectedEventEventStatusEnum = selectedEvent.getEventStatusEnum();
        UUID eventInitiatorUUID = selectedEvent.getEventInitiatorUUID();
        UUID currentUserUUID = Login.getInstance().getCurrentUser().getUserUUID();
        HashMap hashMapConfirmedDates = selectedEvent.getHashMapConfirmedDates();
        DateData acceptedDate = selectedEvent.getAcceptedDate();
        EventMembersList eventMembersList = DBWorker.getInstance(getApplicationContext()).getCertainEventMembersList(selectedEvent.getEventUUID());
        ArrayList<UUID> eventMembersUuidList = eventMembersList.getEventMembersUuidArrayList();

        if ((selectedEventEventStatusEnum.equals(EventStatus.CREATE) || selectedEventEventStatusEnum.equals(EventStatus.UNDER_CONSIDERATION)) && eventInitiatorUUID.equals(currentUserUUID)) {
            Intent intent = new Intent(EventsScreen.this, EventStatusCardActivity.class);
            intent.putExtra(eventNameToTransmit, selectedEvent);
            startActivity(intent);
        } else if (selectedEventEventStatusEnum.equals(EventStatus.CREATE) && !eventInitiatorUUID.equals(currentUserUUID)) {
            Intent intent = new Intent(EventsScreen.this, ConfirmationEventScreenActivity.class);
            intent.putExtra(eventNameToTransmit, selectedEvent);
            startActivity(intent);
        } else if (selectedEventEventStatusEnum.equals(EventStatus.UNDER_CONSIDERATION) && !hashMapConfirmedDates.containsKey(currentUserUUID)) {
            Intent intent = new Intent(EventsScreen.this, ConfirmationEventScreenActivity.class);
            intent.putExtra(eventNameToTransmit, selectedEvent);
            startActivity(intent);
        /*} else if (selectedEventEventStatusEnum.equals(EventStatus.UNDER_CONSIDERATION) && hashMapConfirmedDates.containsKey(currentUserUUID)) {
            Intent intent = new Intent(EventsScreen.this, FinalEventDateChoiseScreenActivity.class);
            intent.putExtra(eventNameToTransmit, selectedEvent);
            startActivity(intent);*/
        } else if (selectedEventEventStatusEnum.equals(EventStatus.UNDER_CONSIDERATION) && eventInitiatorUUID.equals(currentUserUUID)) {
            Intent intent = new Intent(EventsScreen.this, EventStatusCardActivity.class);
            intent.putExtra(eventNameToTransmit, selectedEvent);
            startActivity(intent);
            /* Toast.makeText(this, "Here must be action", Toast.LENGTH_SHORT).show();*/
        } else if (selectedEventEventStatusEnum.equals(EventStatus.APPROVING) && eventInitiatorUUID.equals(currentUserUUID) && acceptedDate == null) {
            Intent intent = new Intent(EventsScreen.this, FinalEventDateChoiseScreenActivity.class);
            intent.putExtra(eventNameToTransmit, selectedEvent);
            startActivity(intent);
        } else if (selectedEventEventStatusEnum.equals(EventStatus.APPROVING) && hashMapConfirmedDates.containsKey(currentUserUUID) && acceptedDate == null) {
            Intent intent = new Intent(EventsScreen.this, EventStatusCardActivity.class);
            intent.putExtra(eventNameToTransmit, selectedEvent);
            startActivity(intent);
        } else if (selectedEventEventStatusEnum.equals(EventStatus.APPROVING) && eventInitiatorUUID.equals(currentUserUUID) && acceptedDate != null) {
            Intent intent = new Intent(EventsScreen.this, EventStatusCardActivity.class);
            intent.putExtra(eventNameToTransmit, selectedEvent);
            startActivity(intent);
        } else if (selectedEventEventStatusEnum.equals(EventStatus.APPROVING) && hashMapConfirmedDates.containsKey(currentUserUUID) && acceptedDate != null) {
            Intent intent = new Intent(EventsScreen.this, FinalEventDateChoiseScreenActivity.class);
            intent.putExtra(eventNameToTransmit, selectedEvent);
            startActivity(intent);
        } else if (selectedEventEventStatusEnum.equals(EventStatus.APPROVING) && hashMapConfirmedDates.containsKey(currentUserUUID)) {
            Intent intent = new Intent(EventsScreen.this, ApprovingActivity.class);
            intent.putExtra(eventNameToTransmit, selectedEvent);
            startActivity(intent); //TODO Think not so stupid check variants for accept date at next line
        } else if (selectedEventEventStatusEnum.equals(EventStatus.ACCEPTED) && eventInitiatorUUID.equals(currentUserUUID) && (selectedEvent.getAcceptedDate().getHour() == 0 && selectedEvent.getAcceptedDate().getMinute() == 0)) {
            Intent intent = new Intent(EventsScreen.this, ApprovingActivity.class);
            intent.putExtra(eventNameToTransmit, selectedEvent);
            startActivity(intent);
        } else if (selectedEventEventStatusEnum.equals(EventStatus.ACCEPTED) && !eventInitiatorUUID.equals(currentUserUUID) && (selectedEvent.getAcceptedDate().getHour() == 0 && selectedEvent.getAcceptedDate().getMinute() == 0)) {
            Toast.makeText(this, R.string.wait_for_party_time, Toast.LENGTH_SHORT).show();
        } else if (selectedEventEventStatusEnum.equals(EventStatus.ACCEPTED) && !eventInitiatorUUID.equals(currentUserUUID) && !(selectedEvent.getAcceptedDate().getHour() == 0 && selectedEvent.getAcceptedDate().getMinute() == 0)) {
            Intent intent = new Intent(EventsScreen.this, ApprovingActivity.class);
            intent.putExtra(eventNameToTransmit, selectedEvent);
            startActivity(intent);
        } else if (selectedEventEventStatusEnum.equals(EventStatus.ACCEPTED) && eventInitiatorUUID.equals(currentUserUUID) && !(selectedEvent.getAcceptedDate().getHour() == 0 && selectedEvent.getAcceptedDate().getMinute() == 0)) {
            Intent intent = new Intent(EventsScreen.this, EventStatusCardActivity.class);
            intent.putExtra(eventNameToTransmit, selectedEvent);
            startActivity(intent);
        } else if (selectedEventEventStatusEnum.equals(EventStatus.ACCEPTED) && hashMapConfirmedDates.containsKey(currentUserUUID)) {
            Toast.makeText(this, "Be ready to party", Toast.LENGTH_SHORT).show();
        } else if (selectedEventEventStatusEnum.equals(EventStatus.SPEND)) {
            Toast.makeText(this, "Event spend", Toast.LENGTH_SHORT).show();
        } else if ((selectedEventEventStatusEnum.equals(EventStatus.REJECTED_BY_USER) && eventMembersUuidList.contains(eventInitiatorUUID) && eventInitiatorUUID.equals(currentUserUUID))) {
            reactForRejectByInitiator(selectedEvent);
        } else if ((selectedEventEventStatusEnum.equals(EventStatus.REJECTED_BY_USER) && eventMembersUuidList.contains(eventInitiatorUUID) && !eventInitiatorUUID.equals(currentUserUUID))) {
            Toast.makeText(this, "Event may be rejected. Please wait.", Toast.LENGTH_SHORT).show();
        } else if ((selectedEventEventStatusEnum.equals(EventStatus.REJECTED_BY_SYSTEM))){
            Toast.makeText(this, "Event rejected by system. No crossing dates for all found.", Toast.LENGTH_SHORT).show();
        }

        //TODO Check here for more variants. Try to simplify thus method.

    }

}
