package com.example.usser.fontances.util;

import android.content.Context;

import com.example.usser.fontances.Event;
import com.example.usser.fontances.User;
import com.example.usser.fontances.data.DBWorker;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import sun.bob.mcalendarview.vo.DateData;

public class EventParser {
    private Event incomeEvent;
    private ArrayList<User> userDataFromDB;
    private static EventParser instance;
    private Context ctx;

    private EventParser (Context incomingContext){
        ctx = incomingContext;
    }

    public ArrayList<String> userNamesFromHashmap(HashMap<UUID, ArrayList<DateData>> hashmapToExtractNames) {
        ArrayList<UUID> usersUUID = getUUIDArrayFromHashmapKeys(hashmapToExtractNames);
        ArrayList<String> userNames = new ArrayList<String>();
        userDataFromDB = DBWorker.getInstance(ctx).getAllUsersArrayListFromBase();
        for (UUID userUuid : usersUUID) {
            for (User user : userDataFromDB) {
                if (userUuid.equals(user.getUserUUID())) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(user.getName())
                            .append(" ")
                            .append(user.getSurname());
                    userNames.add(stringBuilder.toString());

                }
            }

        }
        return userNames;
    }

    public static EventParser getInstance(Context incomingContext) {
        if (instance == null){
            instance = new EventParser(incomingContext);
        }
        return instance;
    }

    private ArrayList<UUID> getUUIDArrayFromHashmapKeys(HashMap<UUID, ArrayList<DateData>> hashmapToExtractNames) {
        ArrayList<UUID> uuidArrayLIST = new ArrayList<UUID>();
        for (Map.Entry<UUID, ArrayList<DateData>> entry : hashmapToExtractNames.entrySet()) {
            //for (int i = 0; i < hashmapToExtractNames.size(); i++){
            uuidArrayLIST.add(entry.getKey());
        }
        return uuidArrayLIST;
    }

}
