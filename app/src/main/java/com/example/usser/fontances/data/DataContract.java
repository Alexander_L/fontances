package com.example.usser.fontances.data;

import android.provider.BaseColumns;

final class DataContract {
    private DataContract() {

    }

    static final class DatabaseEntry implements BaseColumns {
        final static String DATABASE_NAME = "fontances.db";
        final static String DATABASE_VERSION = "2";
        final static String TABLE_NAME_EVENTS = "events_list";
        final static String TABLE_NAME_USERS = "users_list";
        final static String TABLE_NAME_LOGINS = "logins_list";
        final static String TABLE_NAME_EVENTS_MEMBERS_LIST = "events_members_list";

        final static String TRANSMITTING_EVENT_CLASS_NAME = "transmittingEventClassName";

        final static String _ID = BaseColumns._ID;
        final static String EVENT_NAME = "event_name";
        final static String EVENT_UUID = "event_uuid";
        final static String EVENT_OCCASION = "event_occasion";
        final static String EVENT_STATUS = "event_status";
        final static String SUGGESTED_DATE = "suggested_date";
        final static String CONFIRMED_DATES = "confirmed_dates";
        final static String ACCEPTED_DATE = "accepted_date";
        final static String USER_INITIATOR_UUID = "user_initiator_uuid";
        final static String USER_EDITOR_UUID = "user_editor_uuid";
        final static String MAP_OF_CONFIRMED_DATES = "map_of_confirmed_dates";

        final static String USER_SURNAME = "user_surname";
        final static String USER_NAME = "user_name";
        final static String USER_UUID = "user_uuid";
        final static String USER_BIRTHDAY = "user_birthday";
        final static String USER_PATH_TO_AVATAR = "user_path_to_avatar";
        final static String USER_ACTIVE = "user_active";
        final static String [] USER_DATA_COLLECTION_FOR_BUILD_INSTANCE = new String[]{USER_SURNAME, USER_NAME, USER_UUID, USER_BIRTHDAY, USER_PATH_TO_AVATAR, USER_ACTIVE};

        final static String USER_LOGIN = "user_login";
        final static String USER_PASSWORD = "user_password";

        final static String EVENT_MEMBERS_LIST = "event_members_list";
        final static String RECORD_IN_STRING_SEPARATOR = "CUT_RECORD_HERE";
        final static String DATA_IN_RECORD_STRING_SEPARATOR = "##(#@-";
        final static String USER_DATE_AND_TIME_TO_COME = "user_date_and_time_to_come";
        final static String USER_BEHIND_THE_WHEEL = "user_behind_the_wheel";
        final static String USER_PLUS_ONE = "user_plus_one";
        final static String USER_WITH_KIDS = "user_with_kids";

        final static String FULL_DATES_SEPARATOR = "CUT";
        final static String DATES_VALUES_SEPARATOR = "-";
        final static String MAP_UUID_SEPARATOR = "UUID_CUTS_HERE";
        final static String MAP_ENTRIES_SEPARATOR = "GET_OVER_HERE";

        final static String SELECTION_BY_USER_LOGIN_AND_USER_PASSWORD = USER_LOGIN + " = ?"  + " AND " + USER_PASSWORD + " = ?";
        final static String SELECTION_BY_USER_UUID =  USER_UUID + " = ?";
        final static String SELECTION_BY_ACTIVE_TRUE = USER_ACTIVE + " = ?";
        final static String SELECTION_BY_CERTAIN_EVENT_UUID = EVENT_UUID + " = ?";

    }
}
