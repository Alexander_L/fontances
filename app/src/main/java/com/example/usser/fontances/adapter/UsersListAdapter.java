package com.example.usser.fontances.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.usser.fontances.User;
import com.example.usser.fontances.R;

import java.util.ArrayList;
import java.util.Calendar;
/*private String surname;
private String name;
private Calendar birthday;
private String pathToAvatarPicture;*/

public class UsersListAdapter extends RecyclerView.Adapter<UsersListAdapter.UserViewHolder> {

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.user_card, viewGroup, false);
        UserViewHolder userViewHolder = new UserViewHolder(view);
        return userViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder userViewHolder, int i) {
        String userData = userArrayList.get(i).getName() + " " + userArrayList.get(i).getSurname();
        userViewHolder.textViewUserName.setText(userData);
        userViewHolder.textViewUserBirthday.setText(userArrayList.get(i).getBirthday().getYear());
    }

    public User getItem(int position){
        return userArrayList.get(position);
    }

    @Override
    public int getItemCount() {
        return userArrayList.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    static class UserViewHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        TextView textViewUserName;
        TextView textViewUserBirthday;

        UserViewHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.user_cardview);
            textViewUserName = (TextView) itemView.findViewById(R.id.tv_usercard_user_name);
            textViewUserBirthday = (TextView) itemView.findViewById(R.id.tv_usercard_user_birthday);
        }

    }

    private ArrayList<User> userArrayList;

    public UsersListAdapter(ArrayList<User> userArrayList) {
        this.userArrayList = userArrayList;
    }
}
