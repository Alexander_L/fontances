package com.example.usser.fontances.data;

public enum EventStatus {
    CREATE, UNDER_CONSIDERATION, APPROVING, ACCEPTED, SPEND, REJECTED_BY_USER, REJECTED_BY_SYSTEM //Create not ends by ED, as all others.
}
