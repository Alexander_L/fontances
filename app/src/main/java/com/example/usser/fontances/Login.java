package com.example.usser.fontances;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.usser.fontances.data.DBHelper;
import com.example.usser.fontances.data.DBWorker;
import com.example.usser.fontances.testpack.TestDBData;

import java.util.UUID;

public class Login extends AppCompatActivity {
    private static Login instance;
    private DBWorker dbWorker;
    protected static DBHelper dbHelper;
    protected User currentUser = null;
    private String login = null;
    private String password = null;
    private UUID userUUID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_screen);
        instance = this;
        dbHelper = DBHelper.getInstance(getApplicationContext());
        dbWorker = DBWorker.getInstance(getApplicationContext()); //Here error, because jgc cannot destroy this???
    }

    public User getCurrentUser() {
        return currentUser;
    }

    public static Login getInstance() {
        return instance;
    }

    private String getLogin() {
        EditText editTextLogin = findViewById(R.id.editTextLogin);
        return editTextLogin.getText().toString();
    }

    private String getPassword() {
        EditText editTextPassword = findViewById(R.id.editTextPassword);
        return editTextPassword.getText().toString();
    }

    protected void onBtnLoginClick(View view) {
        //Send Strings and boolean, take back boolean;
        if (getLogin().equals("admin") && getPassword().equals("admin")) {
            Intent intent = new Intent(Login.this, UserCreatingActivity.class);
            startActivity(intent);
        }//Is this bad hardcoded hack?
        else {
            if (!isExists()) {
                Toast.makeText(this, R.string.invalid_data_entered, Toast.LENGTH_SHORT).show();
            } else { //TODO Check user logins for doublers
                //currentUser = TestDBData.getInstance().getCurrentUserInfo(login, password);
                setUserUUID(dbWorker.getUserUUIDFromBase(login, password));
                currentUser = dbWorker.getCurrentUserInfoFromBase(getUserUUID());
                Intent intent = new Intent(Login.this, EventsScreen.class);
                startActivity(intent);
            }
        }
    }

    private boolean isExists() {
        login = getLogin();
        password = getPassword();
        return dbWorker.checkUserInBase(login, password);

    }

    private void setUserUUID(UUID userUUIDFromBase) {
        userUUID = userUUIDFromBase;
    }

    public UUID getUserUUID() {
        return userUUID;
    }
}
