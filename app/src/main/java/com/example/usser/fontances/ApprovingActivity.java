package com.example.usser.fontances;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usser.fontances.data.DBWorker;
import com.example.usser.fontances.data.EventStatus;

import java.util.ArrayList;
import java.util.UUID;

import sun.bob.mcalendarview.vo.DateData;

public class ApprovingActivity extends AppCompatActivity {
    private Event transmittedEvent;
    private DateData acceptedDate;
    private EventMembersList transmittedEventMemberList; //TODO Here must be only EventMemberListRecord for change.
    private EventMemberListRecord eventMemberListRecord;

    private EditText timeToCome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approving);
        transmittedEvent = (Event) getIntent().getParcelableExtra(EventsScreen.getInstance().getEventNameToTransmit());
        transmittedEventMemberList = DBWorker.getInstance(getApplicationContext()).getCertainEventMembersList(transmittedEvent.getEventUUID());
        eventMemberListRecord = transmittedEventMemberList.getEventMemberListRecordByMemberUuid(Login.getInstance().currentUser.getUserUUID());
        acceptedDate = transmittedEvent.getAcceptedDate();
        timeToCome = findViewById(R.id.editText_approve_time);
        timeToCome.setFilters(new InputFilter[]{timeFilter});
        TextView confirmationDateText = findViewById(R.id.textView_approve_confirmation_text);
        confirmationDateText.setText(approvingConfirmationText());

    }


    public void onBtnApprovingActivityApproveClick(View view) {
        if(checkForTimeInput()) {
            actionByRole();
            setFlagsInMemberListRecord();
            transmittedEventMemberList.updateMemberDataInList(eventMemberListRecord);

            DBWorker.getInstance(getApplicationContext()).addEventToBase(transmittedEvent);
            DBWorker.getInstance(getApplicationContext()).updateEventMembersListInDatabase(transmittedEventMemberList);

            Toast.makeText(this, R.string.ok, Toast.LENGTH_SHORT).show();
            finish();
        } else{
            Toast.makeText(this, R.string.input_time, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkForTimeInput() {
        return (timeToCome.getText().toString().matches("^(([0,1][0-9])|(2[0-3])):[0-5][0-9]$"));
    }

    private void setFlagsInMemberListRecord(){
        CheckBox isBehindTheWheel = findViewById(R.id.checkBox_behind_the_wheel);
        CheckBox isPlusOne = findViewById(R.id.checkBox_plus_one);
        CheckBox isWithKids = findViewById(R.id.checkBox_with_kids);
        eventMemberListRecord.setBehindTheWheel(isBehindTheWheel.isChecked());
        eventMemberListRecord.setPlusOne(isPlusOne.isChecked());
        eventMemberListRecord.setWithKids(isWithKids.isChecked());
    }


    private void actionByRole(){
        String hoursAndMinutesToCome = timeToCome.getText().toString();
        DateData acceptedDateWithTime = acceptedDate;
        if(Login.getInstance().getCurrentUser().getUserUUID().equals(transmittedEvent.getEventInitiatorUUID())){
            String[] time = hoursAndMinutesToCome.split(":");
            acceptedDateWithTime.setHour(Integer.valueOf(time[0]));
            acceptedDateWithTime.setMinute(Integer.valueOf(time[1]));
            transmittedEvent.setAcceptedDate(acceptedDateWithTime);


        } else{
            String[] time = hoursAndMinutesToCome.split(":");
            acceptedDateWithTime.setHour(Integer.valueOf(time[0]));
            acceptedDateWithTime.setMinute(Integer.valueOf(time[1]));
            ArrayList<DateData> dateDataArrayListFromEvent = new ArrayList<DateData>();
            dateDataArrayListFromEvent.add(acceptedDateWithTime);
            transmittedEvent.setHashMapConfirmedDatesElement(Login.getInstance().getCurrentUser().getUserUUID(), dateDataArrayListFromEvent);
            transmittedEvent.setEventStatusEnum(EventStatus.ACCEPTED);
        }
    }

    private String approvingConfirmationText () {
        StringBuilder stringBuilder = new StringBuilder();
        if (Login.getInstance().currentUser.getUserUUID().equals(transmittedEvent.getEventInitiatorUUID())){
            stringBuilder.append(R.string.approve_confirmation_time_for_initiator_part_one)
                    .append("\n")
                    .append(acceptedDate.getDayString())
                    .append(".")
                    .append(acceptedDate.getMonthString())
                    .append(".")
                    .append(acceptedDate.getYear())
                    .append("\n")
                    .append(R.string.at);
        } else {
            stringBuilder.append(R.string.approve_confirmation_text_part_one)
                    .append("\n")
                    .append(acceptedDate.getDayString())
                    .append(".")
                    .append(acceptedDate.getMonthString())
                    .append(".")
                    .append(acceptedDate.getYear())
                    .append(R.string.at)
                    .append(acceptedDate.getHourString())
                    .append(":")
                    .append(acceptedDate.getMinuteString())
                    .append("\n")
                    .append(R.string.approve_confirmation_text_part_two);
        }
        return stringBuilder.toString();
    }

    InputFilter timeFilter = new InputFilter() { // фильтр ввода. позволяет вводить от 00:00 до 23:59
        boolean doneOnce = false;
        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest,
        int dstart, int dend) {

            if (source.length() > 1 && !doneOnce) {
                source = source.subSequence(source.length() - 1, source.length());
                if (source.charAt(0) >= '0' && source.charAt(0) <= '2') { // первый символ может быть 0,1,2
                    doneOnce = true; //флаг первого символа в editText
                    return source;
                } else {
                    return ""; //остальные символы будут зануляться
                }
            }
            if (source.length() == 0) {
                return null;// deleting, keep original editing
            }
            String result = "";
            result += dest.toString().substring(0, dstart);
            result += source.toString().substring(start, end);
            result += dest.toString().substring(dend, dest.length());
            if (result.length() > 5) { // если в поле 5 символов( 1 2 : 4 5 )
                return "";// не разрешать добавлять символы
            }
            boolean allowEdit = true;
            char c;
            if (result.length() > 0) {
                c = result.charAt(0);
                allowEdit &= (c >= '0' && c <= '2');
            }
            if (result.length() > 1) {
                c = result.charAt(1);
                if (result.charAt(0) == '0' || result.charAt(0) == '1')
                    allowEdit &= (c >= '0' && c <= '9');
                else
                    allowEdit &= (c >= '0' && c <= '3');
            }
            if (result.length() > 2) {
                c = result.charAt(2);
                allowEdit &= (c == ':');
            }
            if (result.length() > 3) {
                c = result.charAt(3);
                allowEdit &= (c >= '0' && c <= '5');
            }
            if (result.length() > 4) {
                c = result.charAt(4);
                allowEdit &= (c >= '0' && c <= '9');
            }
            return allowEdit ? null : "";
        }
    };


}
