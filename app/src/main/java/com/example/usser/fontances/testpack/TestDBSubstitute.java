package com.example.usser.fontances.testpack;

import com.example.usser.fontances.Event;

import java.util.ArrayList;

//this class emulates DataBase
public class TestDBSubstitute {
    static TestDBSubstitute instance;
    static ArrayList<Event> eventArrayList = new ArrayList<Event>();

    static TestDBSubstitute getInstance(){
        if (instance == null){
            instance = new TestDBSubstitute();
        }
        return instance;
    }

    public ArrayList<Event> getEventArrayList(){
        return eventArrayList;
    }

    public void addEventToArrayList(Event addEvent){
        eventArrayList.add(addEvent);
    }

}
