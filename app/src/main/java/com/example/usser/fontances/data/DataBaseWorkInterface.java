package com.example.usser.fontances.data;

import android.database.Cursor;

import com.example.usser.fontances.Event;
import com.example.usser.fontances.EventMembersList;
import com.example.usser.fontances.User;

import java.util.ArrayList;
import java.util.UUID;

public interface DataBaseWorkInterface {

    boolean checkUserInBase(String login, String password);

    void addUserInBase (User createdUser);

    UUID getUserUUIDFromBase(String login, String password);

    User getCurrentUserInfoFromBase (UUID userUUID);

    ArrayList<User> getAllUsersArrayListFromBase();

    ArrayList<Event> getAllEventsRecordsArrayListFromBase (); //THis will select ALL saves from base, will be many copied events with "raznye" status.

    void addEventToBase(Event createdEvent);

    void addLoginInBase(String login, String password, UUID userUUID);

    ArrayList<UUID> getActiveUsersUuidArrayListFromBase();

    void addEventMembersListToBase (EventMembersList eventMembersList);

    EventMembersList getCertainEventMembersList(UUID eventUUID);

    ArrayList<EventMembersList> getAllEventsMembersList();

    ArrayList<Event> getCreatedEvents();

    void updateEventMembersListInDatabase (EventMembersList updatedEventMemberList);

    String getUserNameSurnameInStringByUuid(UUID userUuid);


}
