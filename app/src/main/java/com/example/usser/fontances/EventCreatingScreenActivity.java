package com.example.usser.fontances;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.usser.fontances.data.DBWorker;
import com.example.usser.fontances.data.EventOccasion;
import com.example.usser.fontances.data.EventStatus;
import com.example.usser.fontances.testpack.TestDBData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.Calendar;

import sun.bob.mcalendarview.MCalendarView;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.vo.DateData;

public class EventCreatingScreenActivity extends AppCompatActivity {
    /* private String eventName;
     private Enum<EventOccasion> eventOccasionEnum;
     private Enum<EventStatus> eventStatusEnum;
     private Calendar suggestedDate;
     private Calendar acceptedDate;
     private ArrayList<Calendar> confirmedDates;
     private User eventInitiator;*/
    //long selectedDate = 0L;
    private ArrayList<DateData> markedDates;
    private boolean dateMarked;
    private Spinner spinner;
    //private ArrayList<DateData> suggestedDates;
    private MCalendarView calendarView;
    private static final int INDEX_OF_ONE_DAY = 0;
    EditText editTextEventName;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_creating_screen);
        editTextEventName = findViewById(R.id.editText_event_name);
        spinner = (Spinner) findViewById(R.id.spinner_occasion_event_creating);
        spinner.setAdapter(new ArrayAdapter<EventOccasion>(this, android.R.layout.simple_list_item_1, EventOccasion.values()));
        //suggestedDate = Calendar.getInstance();
        markedDates = new ArrayList<>();
        //calendarView = new MCalendarView(this);
        calendarView = ((MCalendarView) findViewById(R.id.event_creating_MCalendarView));
        /*ArrayList<DateData> dates = new ArrayList<>();
        dates.add(new DateData(2018, 10, 26));
        dates.add(new DateData(2018, 10, 27));

        for (int i = 0; i < dates.size(); i++) {
            calendarView.markDate(dates.get(i).getYear(), dates.get(i).getMonth(), dates.get(i).getDay());//mark multiple dates with this code.
        }
*/
        calendarView.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(View view, DateData date) {
                dateMarked = false;
                int m = 0;
                for (DateData i : markedDates
                        ) {
                    if (i.equals(date)) {
                        dateMarked = true;
                        m = markedDates.indexOf(i);
                    }
                }
                if (!dateMarked) {
                    calendarView.markDate(date);
                    markedDates.add(date);
                }
                else if(dateMarked){
                    markedDates.remove(m);
                    calendarView.unMarkDate(date);
                }
                Toast.makeText(EventCreatingScreenActivity.this, String.format("%d-%d", date.getDay(), date.getMonth()), Toast.LENGTH_SHORT).show();
            }
        });

        /*final CalendarView calendarView = findViewById(R.id.calendarView_event_creating);
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                suggestedDate.setTimeInMillis(calendarView.getDate());
            }
        });*/
    }

    protected void onBtnCreateEventClick(View view) {
       /* if (oneCreatedDay()){
            Toast.makeText(this, R.string.select_only_one_day, Toast.LENGTH_SHORT).show();
        }
        else {
            //calendarView.unMarkDate(suggestedDate);
            calendarView.unMarkDate(markedDates.get(INDEX_OF_ONE_DAY));
            Event createdEvent = getDataToEventCreate();
            TestDBData.getInstance().addEventToBase(createdEvent);
            //DBWorker.getInstance().addEventToBase(createdEvent);
            EventsScreen.getInstance().notifyAdapterDataSetChanged();
            finish();
        }*/

       if (isAllFieldsNotNull()) {
           Event createdEvent = getDataToEventCreate();
           //TestDBData.getInstance().addEventToBase(createdEvent);
           EventMembersList createdEventMembersList = getDataToEventMemberListCreate(createdEvent.getEventUUID());
           DBWorker.getInstance(getApplicationContext()).addEventToBase(createdEvent);
           DBWorker.getInstance(getApplicationContext()).addEventMembersListToBase(createdEventMembersList);
           Toast.makeText(this, R.string.ok, Toast.LENGTH_SHORT).show();
           EventsScreen.getInstance().notifyAdapterDataSetChanged();
           finish();
       }

    }

    private boolean oneCreatedDay(){
        return markedDates.size() != 0 && markedDates.size() > 1;
    }


    protected Event getDataToEventCreate() { //Too big method?
        String eventName = String.valueOf(editTextEventName.getText());
        //Enum<EventOccasion> eventOccasionEnum = (Enum<EventOccasion>) ((findViewById(R.id.spinner_occasion_event_creating)).getSelectedItem());  Do not works
        UUID eventUUID = UUID.randomUUID();
        Enum<EventOccasion> eventOccasionEnum = (Enum<EventOccasion>) spinner.getSelectedItem();
        Enum<EventStatus> statusEnum = EventStatus.CREATE;
        ArrayList<DateData> suggestedDatesArrayList = calendarView.getMarkedDates().getAll();
        //suggestedDate = markedDates.get(INDEX_OF_ONE_DAY);
        HashMap <UUID, ArrayList<DateData>> hashMapConfirmedDates = new HashMap<>();
        //suggestedDate.setTimeInMillis(selectedDate);
        Toast.makeText(this, R.string.ok, Toast.LENGTH_SHORT).show();

        return new Event(eventName, eventUUID, eventOccasionEnum, statusEnum, suggestedDatesArrayList, null, null, Login.getInstance().getCurrentUser().getUserUUID(), null, hashMapConfirmedDates);

    }

    private EventMembersList getDataToEventMemberListCreate(UUID createdEventUuid){
        ArrayList<UUID> activeUsersUuidArayList = DBWorker.getInstance(getApplicationContext()).getActiveUsersUuidArrayListFromBase();
        ArrayList<EventMemberListRecord> eventMembersListRecordsArrayList = new ArrayList<>();
        for(UUID eventMemberUuid : activeUsersUuidArayList){
            EventMemberListRecord eventMemberListRecord = new EventMemberListRecord();
            eventMemberListRecord.setEventMemberUuid(eventMemberUuid);
            eventMembersListRecordsArrayList.add(eventMemberListRecord);
        }
        EventMembersList eventMembersList = new EventMembersList();
        eventMembersList.setEventUUID(createdEventUuid);
        eventMembersList.setEventMemberDataArrayList(eventMembersListRecordsArrayList);
        return eventMembersList;
    }

    private boolean isAllFieldsNotNull () {
        boolean allFieldsNotNull = true;
        if (editTextEventName.getText().length() == 0 ){
            allFieldsNotNull = false;
            Toast.makeText(this, R.string.fill_event_occasion_field, Toast.LENGTH_SHORT).show();
        }
        else if (calendarView.getMarkedDates().getAll().isEmpty()){
            allFieldsNotNull = false;
            Toast.makeText(this, R.string.select_one_or_more_event_dates, Toast.LENGTH_SHORT).show();
        }

        return allFieldsNotNull;
    }

}