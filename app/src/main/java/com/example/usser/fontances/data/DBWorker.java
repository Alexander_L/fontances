package com.example.usser.fontances.data;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.usser.fontances.Event;
import com.example.usser.fontances.EventMemberListRecord;
import com.example.usser.fontances.EventMembersList;
import com.example.usser.fontances.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

import sun.bob.mcalendarview.vo.DateData;

public class DBWorker implements DataBaseWorkInterface {

    private DBWorker(Context context) {
        ctx = context;
        dbHelper = DBHelper.getInstance(context);
        sqLiteDatabase = dbHelper.getWritableDatabase();
    }

    private DBHelper dbHelper;
    private static DBWorker instance;
    private static Context ctx;
    private SQLiteDatabase sqLiteDatabase;

    public static DBWorker getInstance(Context contextForInstance) {
      /*  Context ctx = null;
        ctx.getApplicationContext(); //I don`t now, how it correct.
        if (instance == null){
            instance = new DBWorker(ctx);
        }*/
        //TODO Check here for context!!!
        if (instance == null) {
            instance = new DBWorker(contextForInstance);
        }
        return instance;
    }


    @Override
    public boolean checkUserInBase(String login, String password) {
        boolean inBase = false;
        Cursor cursorUserInBase = sqLiteDatabase.query(
                DataContract.DatabaseEntry.TABLE_NAME_LOGINS,
                new String[]{DataContract.DatabaseEntry.USER_UUID},
                DataContract.DatabaseEntry.SELECTION_BY_USER_LOGIN_AND_USER_PASSWORD,
                new String[]{login, password},
                null,
                null,
                null);
        if(cursorUserInBase.getCount() !=0)
        {inBase = true;}
        //inBase = !cursorUserInBase.isNull(cursorUserInBase.getColumnIndex(DataContract.DatabaseEntry.USER_UUID)); //TODO Check here for selection (zabyl perevod "uslovie").

        cursorUserInBase.close();
        return inBase;
    }

    @Override
    public UUID getUserUUIDFromBase(String login, String password) {
        UUID userUuidFromBase;
        Cursor cursorWithUserUUID = sqLiteDatabase.query(DataContract.DatabaseEntry.TABLE_NAME_LOGINS,
                new String[]{DataContract.DatabaseEntry.USER_UUID},
                DataContract.DatabaseEntry.SELECTION_BY_USER_LOGIN_AND_USER_PASSWORD, new String[]{login, password},
                null,
                null,
                null); //TODO Here must be selector from Database Here ugly hardcode for tablenames!
        cursorWithUserUUID.moveToFirst();
        userUuidFromBase = UUID.fromString(cursorWithUserUUID.getString(cursorWithUserUUID.getColumnIndex(DataContract.DatabaseEntry.USER_UUID)));
        cursorWithUserUUID.close();

        return userUuidFromBase;
    }

    @Override
    public User getCurrentUserInfoFromBase(UUID incomeUserUUID) { //TODO Here must be selector from database too.
        User userFromBase;
        String [] dataUserForCollect = DataContract.DatabaseEntry.USER_DATA_COLLECTION_FOR_BUILD_INSTANCE;
        Cursor cursorWithCurrentUserInfo = sqLiteDatabase.query(
                DataContract.DatabaseEntry.TABLE_NAME_USERS,
                dataUserForCollect,// TODO Here and around check getting columns for primary key
                DataContract.DatabaseEntry.SELECTION_BY_USER_UUID, new String[]{incomeUserUUID.toString()},
                null,
                null,
                null
        );
        cursorWithCurrentUserInfo.moveToFirst();
        String userSurnameFromBase = cursorWithCurrentUserInfo.getString(cursorWithCurrentUserInfo.getColumnIndex(DataContract.DatabaseEntry.USER_SURNAME));
        String userNameFromBase = cursorWithCurrentUserInfo.getString(cursorWithCurrentUserInfo.getColumnIndex(DataContract.DatabaseEntry.USER_NAME));
        UUID userUUIDFromBase = UUID.fromString(cursorWithCurrentUserInfo.getString(cursorWithCurrentUserInfo.getColumnIndex(DataContract.DatabaseEntry.USER_UUID)));
        DateData userBirthdayFromBase = stringFormBaseToDateData(cursorWithCurrentUserInfo.getString(cursorWithCurrentUserInfo.getColumnIndex(DataContract.DatabaseEntry.USER_BIRTHDAY)));
        String userPathToAvatarFromBase = cursorWithCurrentUserInfo.getString(cursorWithCurrentUserInfo.getColumnIndex(DataContract.DatabaseEntry.USER_PATH_TO_AVATAR));
        boolean userActiveFromBase = Boolean.getBoolean(cursorWithCurrentUserInfo.getString(cursorWithCurrentUserInfo.getColumnIndex(DataContract.DatabaseEntry.USER_ACTIVE))); //TODO check here for correct boolean getting!
        userFromBase = new User(userSurnameFromBase, userNameFromBase, userUUIDFromBase, userBirthdayFromBase, userPathToAvatarFromBase, userActiveFromBase);

        cursorWithCurrentUserInfo.close();

        return userFromBase;
    }

    @Override
    public ArrayList<Event> getCreatedEvents() { //TODO release method
        return null;
    }

    @Override
    public void addEventToBase(Event createdEvent) {
        ContentValues contentValues = new ContentValues();
        String eventName = createdEvent.getEventName();
        String eventUUID = createdEvent.getEventUUID().toString();
        String eventOccasion = createdEvent.getEventOccasionEnum().toString();
        String eventStatus = createdEvent.getEventStatusEnum().toString();
        String suggestedDate = stringToBaseFromArrayListDateData(createdEvent.getSuggestedDates());
        String confirmedDates = stringToBaseFromArrayListDateData(createdEvent.getConfirmedDates());
        String acceptedDate = stringToBaseFromDateData(createdEvent.getAcceptedDate());
                /*+ DataContract.DatabaseEntry.DATES_VALUES_SEPARATOR
                + String.valueOf(createdEvent.getAcceptedDate().getMonth())
                + DataContract.DatabaseEntry.DATES_VALUES_SEPARATOR
                + String.valueOf(createdEvent.getAcceptedDate().getDay());*/
        String userInitiatorUUID = createdEvent.getEventInitiatorUUID().toString();
        String userEditorUUID = createdEvent.getEventEditorUUID().toString();
        String haspMapConfirmedDates = stringToBaseFromHashMapUuidAndDates(createdEvent.getHashMapConfirmedDates());

        contentValues.put(DataContract.DatabaseEntry.EVENT_NAME, eventName);
        contentValues.put(DataContract.DatabaseEntry.EVENT_UUID, eventUUID);
        contentValues.put(DataContract.DatabaseEntry.EVENT_OCCASION, eventOccasion);
        contentValues.put(DataContract.DatabaseEntry.EVENT_STATUS, eventStatus);
        contentValues.put(DataContract.DatabaseEntry.SUGGESTED_DATE, suggestedDate);
        contentValues.put(DataContract.DatabaseEntry.CONFIRMED_DATES, confirmedDates);
        contentValues.put(DataContract.DatabaseEntry.ACCEPTED_DATE, acceptedDate);
        contentValues.put(DataContract.DatabaseEntry.USER_INITIATOR_UUID, userInitiatorUUID);
        contentValues.put(DataContract.DatabaseEntry.USER_EDITOR_UUID, userEditorUUID);
        contentValues.put(DataContract.DatabaseEntry.MAP_OF_CONFIRMED_DATES, haspMapConfirmedDates);

        sqLiteDatabase.insert(DataContract.DatabaseEntry.TABLE_NAME_EVENTS, null, contentValues);

    }

    @Override
    public ArrayList<Event> getAllEventsRecordsArrayListFromBase() {
        ArrayList<Event> eventsFromBase = new ArrayList<Event>();
        Event event;
        Cursor cursor = sqLiteDatabase.query(DataContract.DatabaseEntry.TABLE_NAME_EVENTS, null, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            event = getEventFromCursor(cursor);
            eventsFromBase.add(event);
            cursor.moveToNext();//TODO Look, this is correct step ?
        }
        return eventsFromBase;
    }

    @Override
    public void addLoginInBase(String login, String password, UUID userUUID) {
        ContentValues contentValues = new ContentValues();
        String userUUIDToBase = userUUID.toString();
        contentValues.put(DataContract.DatabaseEntry.USER_LOGIN, login);
        contentValues.put(DataContract.DatabaseEntry.USER_PASSWORD, password);
        contentValues.put(DataContract.DatabaseEntry.USER_UUID, userUUIDToBase);

        sqLiteDatabase.insert(DataContract.DatabaseEntry.TABLE_NAME_LOGINS, null, contentValues);
    }

    @Override
    public void addUserInBase(User createdUser) {
        ContentValues contentValues = new ContentValues();
        String userName = createdUser.getName();
        String userSurname = createdUser.getSurname();
        String userBirthDate = stringToBaseFromDateData(createdUser.getBirthday());
        String userUUID = createdUser.getUserUUID().toString();
        String userPathToAvatar = createdUser.getPathToAvatarPicture();
        boolean userActive = createdUser.isUserActive();

        contentValues.put(DataContract.DatabaseEntry.USER_NAME, userName);
        contentValues.put(DataContract.DatabaseEntry.USER_SURNAME, userSurname);
        contentValues.put(DataContract.DatabaseEntry.USER_BIRTHDAY, userBirthDate);
        contentValues.put(DataContract.DatabaseEntry.USER_UUID, userUUID);
        contentValues.put(DataContract.DatabaseEntry.USER_PATH_TO_AVATAR, userPathToAvatar);
        contentValues.put(DataContract.DatabaseEntry.USER_ACTIVE, userActive);

        sqLiteDatabase.insert(DataContract.DatabaseEntry.TABLE_NAME_USERS, null, contentValues);


    }

    @Override
    public ArrayList<User> getAllUsersArrayListFromBase() {
        Cursor cursorAllUsersFromBase = sqLiteDatabase.query(DataContract.DatabaseEntry.TABLE_NAME_USERS,
                null,
                null,
                null,
                null,
                null,
                null);
        cursorAllUsersFromBase.moveToFirst();
        ArrayList<User> allUsersFromBase = new ArrayList<User>();
        while (!cursorAllUsersFromBase.isAfterLast()) {
            allUsersFromBase.add(getUserFromCursor(cursorAllUsersFromBase));
            cursorAllUsersFromBase.moveToNext();//TODO Look, this is correct step ?
        }
        cursorAllUsersFromBase.close();
        return allUsersFromBase;
    }

    @Override
    public ArrayList<UUID> getActiveUsersUuidArrayListFromBase() {
        Cursor cursorAllActiveUuidUsersFromBase = sqLiteDatabase.query(DataContract.DatabaseEntry.TABLE_NAME_USERS,
                new String[]{DataContract.DatabaseEntry.USER_UUID, DataContract.DatabaseEntry.USER_ACTIVE},
                DataContract.DatabaseEntry.SELECTION_BY_ACTIVE_TRUE, new String[]{"1"},
                null,
                null,
                null,
                null);

        cursorAllActiveUuidUsersFromBase.moveToFirst();
        ArrayList<UUID> allActiveUsersUuidFromBase = new ArrayList<UUID>();
        while (!cursorAllActiveUuidUsersFromBase.isAfterLast()) {
            allActiveUsersUuidFromBase.add(UUID.fromString(cursorAllActiveUuidUsersFromBase.getString(cursorAllActiveUuidUsersFromBase.getColumnIndex(DataContract.DatabaseEntry.USER_UUID))));
            cursorAllActiveUuidUsersFromBase.moveToNext();//TODO Look, this is correct step ?
        }

        cursorAllActiveUuidUsersFromBase.close();
        return allActiveUsersUuidFromBase;
    }

    @Override
    public void addEventMembersListToBase(EventMembersList eventMembersList) {
        //StringBuilder stringBuilder = new StringBuilder();
        ContentValues contentValues = new ContentValues();
        String eventUuidInString = eventMembersList.getEventUUID().toString();
        /*for (EventMemberListRecord record : eventMembersList.getEventMemberDataArrayList()
                ) {
            stringBuilder.append(setEventMemberListRecordToStringForBase(record))
                    .append(DataContract.DatabaseEntry.RECORD_IN_STRING_SEPARATOR);
        }*/
        //String membersListInString = stringBuilder.toString();
        String membersListInString = getEventMembersListArrayDataToStringForDatabase(eventMembersList.getEventMemberDataArrayList());
        contentValues.put(DataContract.DatabaseEntry.EVENT_UUID, eventUuidInString);
        contentValues.put(DataContract.DatabaseEntry.EVENT_MEMBERS_LIST, membersListInString);
        sqLiteDatabase.insert(DataContract.DatabaseEntry.TABLE_NAME_EVENTS_MEMBERS_LIST, null, contentValues);


    }

    @Override
    public ArrayList<EventMembersList> getAllEventsMembersList() {
        Cursor cursor = sqLiteDatabase.query(DataContract.DatabaseEntry.TABLE_NAME_EVENTS_MEMBERS_LIST,
                new String[]{DataContract.DatabaseEntry.EVENT_UUID, DataContract.DatabaseEntry.EVENT_MEMBERS_LIST},
                null, null, null, null, null);
        cursor.close();
        cursor.moveToFirst();
        ArrayList<EventMembersList> allEventsMembersListArrayList = new ArrayList<>();
        while (!cursor.isAfterLast()) {
            String eventUuidInString = cursor.getString(cursor.getColumnIndex(DataContract.DatabaseEntry.EVENT_UUID)).toString();
            String eventMembersDataInString = cursor.getString(cursor.getColumnIndex(DataContract.DatabaseEntry.EVENT_MEMBERS_LIST));
            UUID eventUuid = UUID.fromString(eventUuidInString);
            ArrayList<EventMemberListRecord> eventMemberListRecordArrayList = new ArrayList<EventMemberListRecord>();
            eventMemberListRecordArrayList = getEventMemberListRecordsArrayListFromStringFromBase(eventMembersDataInString);
            EventMembersList eventMembersList = new EventMembersList();
            eventMembersList.setEventUUID(eventUuid);
            eventMembersList.setEventMemberDataArrayList(eventMemberListRecordArrayList);
            allEventsMembersListArrayList.add(eventMembersList);
        }
        cursor.close();
        return allEventsMembersListArrayList;
    }

    @Override
    public EventMembersList getCertainEventMembersList(UUID eventUUID) {
        EventMembersList eventMembersList = new EventMembersList();
        Cursor cursor = sqLiteDatabase.query(DataContract.DatabaseEntry.TABLE_NAME_EVENTS_MEMBERS_LIST,
                new String[]{DataContract.DatabaseEntry.EVENT_UUID, DataContract.DatabaseEntry.EVENT_MEMBERS_LIST},
                DataContract.DatabaseEntry.SELECTION_BY_CERTAIN_EVENT_UUID, new String[]{eventUUID.toString()},
                null,
                null,
                null
        );
        cursor.moveToFirst();
        String eventUuidInString = cursor.getString(cursor.getColumnIndex(DataContract.DatabaseEntry.EVENT_UUID));
        String eventMembersListInString = cursor.getString(cursor.getColumnIndex(DataContract.DatabaseEntry.EVENT_MEMBERS_LIST));
        eventMembersList.setEventUUID(UUID.fromString(eventUuidInString));
        eventMembersList.setEventMemberDataArrayList(getEventMemberListRecordsArrayListFromStringFromBase(eventMembersListInString));
        cursor.close();
        return eventMembersList;
    }

    @Override
    public void updateEventMembersListInDatabase(EventMembersList updatedEventMemberList) {
        String eventUuid = updatedEventMemberList.getEventUUID().toString();
        String updatingEventMemberListInString = getEventMembersListArrayDataToStringForDatabase(updatedEventMemberList.getEventMemberDataArrayList());
        setUpdateEventMembersListInDatabase(eventUuid, updatingEventMemberListInString);

    }

    private ArrayList<EventMemberListRecord> getEventMemberListRecordsArrayListFromStringFromBase(String recordStringFromBase) {
        ArrayList<EventMemberListRecord> eventMemberListRecordArrayList = new ArrayList<>();
        String[] everyMemberRecordsToCompile = recordStringFromBase.split(DataContract.DatabaseEntry.RECORD_IN_STRING_SEPARATOR); //Array size must be == EventMemberListRecord fields number.
        for (String recordToCompile : everyMemberRecordsToCompile
                ) {
            String[] record = recordToCompile.split(DataContract.DatabaseEntry.DATA_IN_RECORD_STRING_SEPARATOR);
            EventMemberListRecord eventMemberListRecord = new EventMemberListRecord();
            eventMemberListRecord.setEventMemberUuid(UUID.fromString(record[0]));
            eventMemberListRecord.setDateAndTimeToCome(stringFormBaseToDateData(record[1]));
            eventMemberListRecord.setBehindTheWheel(Boolean.parseBoolean(record[2]));
            eventMemberListRecord.setPlusOne(Boolean.parseBoolean(record[3]));
            eventMemberListRecord.setWithKids(Boolean.parseBoolean(record[4]));
            eventMemberListRecordArrayList.add(eventMemberListRecord);
        }
        return eventMemberListRecordArrayList;

    }


    private String setEventMemberListRecordToStringForBase(EventMemberListRecord eventMemberListRecord) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(eventMemberListRecord.getEventMemberUuid().toString())
                .append(DataContract.DatabaseEntry.DATA_IN_RECORD_STRING_SEPARATOR)
                .append(stringToBaseFromDateData(eventMemberListRecord.getDateAndTimeToCome()))
                .append(DataContract.DatabaseEntry.DATA_IN_RECORD_STRING_SEPARATOR)
                .append(eventMemberListRecord.isBehindTheWheel())
                .append(DataContract.DatabaseEntry.DATA_IN_RECORD_STRING_SEPARATOR)
                .append(eventMemberListRecord.isPlusOne())
                .append(DataContract.DatabaseEntry.DATA_IN_RECORD_STRING_SEPARATOR)
                .append(eventMemberListRecord.isWithKids());
        return stringBuilder.toString();
    }


    private User getUserFromCursor(Cursor userCursor) {
        String userSurnameFromBase = userCursor.getString(userCursor.getColumnIndex(DataContract.DatabaseEntry.USER_SURNAME));
        String userNameFromBase = userCursor.getString(userCursor.getColumnIndex(DataContract.DatabaseEntry.USER_NAME));
        DateData userBirthdayFromBase = stringFormBaseToDateData(userCursor.getString(userCursor.getColumnIndex(DataContract.DatabaseEntry.USER_BIRTHDAY)));
        UUID userUUIDFromBase = UUID.fromString(userCursor.getString(userCursor.getColumnIndex(DataContract.DatabaseEntry.USER_UUID)));
        String userPathToAvatearFromBase = userCursor.getString(userCursor.getColumnIndex(DataContract.DatabaseEntry.USER_PATH_TO_AVATAR));
        boolean userActiveFromBase = Boolean.getBoolean(userCursor.getString(userCursor.getColumnIndex(DataContract.DatabaseEntry.USER_ACTIVE)));
        return new User(userSurnameFromBase, userNameFromBase, userUUIDFromBase, userBirthdayFromBase, userPathToAvatearFromBase, userActiveFromBase);
    }

    private Event getEventFromCursor(Cursor eventCursorFromBase) { //Hardcoded arrays index - is it correct?
        String eventNameString = eventCursorFromBase.getString(eventCursorFromBase.getColumnIndex(DataContract.DatabaseEntry.EVENT_NAME));
        String eventUUIDString = eventCursorFromBase.getString(eventCursorFromBase.getColumnIndex(DataContract.DatabaseEntry.EVENT_UUID));
        UUID eventUUID = UUID.fromString(eventUUIDString);
        String eventOccasionString = eventCursorFromBase.getString(eventCursorFromBase.getColumnIndex(DataContract.DatabaseEntry.EVENT_OCCASION));
        Enum<EventOccasion> eventOccasionEnum = EventOccasion.valueOf(eventOccasionString);
        String eventStatusString = eventCursorFromBase.getString(eventCursorFromBase.getColumnIndex(DataContract.DatabaseEntry.EVENT_STATUS));
        Enum<EventStatus> eventStatusEnum = EventStatus.valueOf(eventStatusString);
        String suggestedDateString = eventCursorFromBase.getString(eventCursorFromBase.getColumnIndex(DataContract.DatabaseEntry.SUGGESTED_DATE));
        ArrayList<DateData> suggestedDatesArrayList = stringFromBaseToArrayListDateData(suggestedDateString);

        /*String[] suggestedDatesStringArray = suggestedDateString.split(DataContract.DatabaseEntry.FULL_DATES_SEPARATOR);
        for (String suggestedDateNotSeparated : suggestedDatesStringArray) {
            String [] date = suggestedDateNotSeparated.split(DataContract.DatabaseEntry.FULL_DATES_SEPARATOR);
        }*/
        /*String[] suggestedDateDataArray = suggestedDateString.split("-");
        DateData suggestedDate = new DateData(Integer.parseInt(suggestedDateDataArray[0]), Integer.parseInt(suggestedDateDataArray[1]), Integer.parseInt(suggestedDateDataArray[2]));  //Year, month, day*/
/*        String confirmedDatesString = eventCursorFromBase.getString(eventCursorFromBase.getColumnIndex(DataContract.DatabaseEntry.CONFIRMED_DATES));
        ArrayList<DateData> confirmedDates = new ArrayList<>();
        String[] confirmedDatesStringArray = confirmedDatesString.split(DataContract.DatabaseEntry.FULL_DATES_SEPARATOR);
        for (String dateNotSeparated : confirmedDatesStringArray) {
            String[] date = dateNotSeparated.split(DataContract.DatabaseEntry.FULL_DATES_SEPARATOR);
            DateData confirmedDate = new DateData(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2])); //Year, month, day
            confirmedDates.add(confirmedDate);
        }*/
        String confirmedDatesString = eventCursorFromBase.getString(eventCursorFromBase.getColumnIndex(DataContract.DatabaseEntry.CONFIRMED_DATES));
        ArrayList<DateData> confirmedDatesArrayList = stringFromBaseToArrayListDateData(confirmedDatesString);
        String acceptedDateString = eventCursorFromBase.getString(eventCursorFromBase.getColumnIndex(DataContract.DatabaseEntry.ACCEPTED_DATE));
        String[] acceptedDateDataArray = acceptedDateString.split("-");
        DateData acceptedDate = new DateData(Integer.parseInt(acceptedDateDataArray[0]), Integer.parseInt(acceptedDateDataArray[1]), Integer.parseInt(acceptedDateDataArray[2]));
        String userInitiatorUUIDString = eventCursorFromBase.getString(eventCursorFromBase.getColumnIndex(DataContract.DatabaseEntry.USER_INITIATOR_UUID));
        UUID userInitiatorUUID = UUID.fromString(userInitiatorUUIDString);
        String userEditorUUIDString = eventCursorFromBase.getString(eventCursorFromBase.getColumnIndex(DataContract.DatabaseEntry.USER_EDITOR_UUID));
        UUID userEditorUUID = UUID.fromString(userEditorUUIDString);
        HashMap<UUID, ArrayList<DateData>> hashMapConfirmedDates = uuidArrayListHashMapFromBaseString(eventCursorFromBase.getString(eventCursorFromBase.getColumnIndex(DataContract.DatabaseEntry.MAP_OF_CONFIRMED_DATES)));
        return new Event(eventNameString, eventUUID, eventOccasionEnum, eventStatusEnum, suggestedDatesArrayList, confirmedDatesArrayList, acceptedDate, userInitiatorUUID, userEditorUUID, hashMapConfirmedDates);
    }

    private String datesArrayListFormattedToStringToBase(ArrayList<DateData> confirmedDatesArray) { //double stringToBaseFromArrayListDateData()
        String dates = null;
        StringBuilder stringBuilder = new StringBuilder(); //TODO Check here and in stringToBaseFromDateData() for correct using StringBuilder;
        for (DateData x : confirmedDatesArray
                ) {
            /*stringBuilder.append(String.valueOf(x.getYear()))
                    .append(DataContract.DatabaseEntry.DATES_VALUES_SEPARATOR)
                    .append(String.valueOf(x.getMonth()))
                    .append(DataContract.DatabaseEntry.DATES_VALUES_SEPARATOR)
                    .append(String.valueOf(x.getDay()))
                    .append(DataContract.DatabaseEntry.DATES_VALUES_SEPARATOR);*/
            stringBuilder.append(stringToBaseFromDateData(x));
        }
        dates = stringBuilder.toString();
        return dates;
    }

    private DateData stringFormBaseToDateData(String notSeparatedDateFromBase) {
        String[] dateParts = notSeparatedDateFromBase.split(DataContract.DatabaseEntry.DATES_VALUES_SEPARATOR);
        DateData dataFromBase = new DateData(Integer.valueOf(dateParts[0]), Integer.valueOf(dateParts[1]), Integer.valueOf(dateParts[2]));
        dataFromBase.setHour(Integer.valueOf(dateParts[3]));
        dataFromBase.setMinute(Integer.valueOf(dateParts[4]));
        return dataFromBase;
    }

    private String stringToBaseFromDateData(DateData dateToSeparate) {
        String date = null;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.valueOf(dateToSeparate.getYear()))
                .append(DataContract.DatabaseEntry.DATES_VALUES_SEPARATOR)
                .append(String.valueOf(dateToSeparate.getMonth()))
                .append(DataContract.DatabaseEntry.DATES_VALUES_SEPARATOR)
                .append(String.valueOf(dateToSeparate.getDay()))
                .append(DataContract.DatabaseEntry.DATES_VALUES_SEPARATOR)
                .append(String.valueOf(dateToSeparate.getHour()))
                .append(DataContract.DatabaseEntry.DATES_VALUES_SEPARATOR)
                .append(String.valueOf(dateToSeparate.getMinute()));
                //.append(DataContract.DatabaseEntry.FULL_DATES_SEPARATOR);
        date = stringBuilder.toString();

/*        return String.valueOf(dateToSeparate.getYear())
                + DataContract.DatabaseEntry.DATES_VALUES_SEPARATOR
                + String.valueOf(dateToSeparate.getMonth())
                + DataContract.DatabaseEntry.DATES_VALUES_SEPARATOR
                + String.valueOf(dateToSeparate.getDay())
                + DataContract.DatabaseEntry.DATES_VALUES_SEPARATOR
                + DataContract.DatabaseEntry.FULL_DATES_SEPARATOR;*/
        return date;
    }


    private ArrayList<DateData> stringFromBaseToArrayListDateData(String notSeparatedString) {
        ArrayList<DateData> formattedDatesArrayList = new ArrayList<>();
        String[] notSeparatedDatesInString = notSeparatedString.split(DataContract.DatabaseEntry.FULL_DATES_SEPARATOR);
        for (String stringWithDate : notSeparatedDatesInString) {
            formattedDatesArrayList.add(stringFormBaseToDateData(stringWithDate));
        }
        return formattedDatesArrayList;
    }

    private String stringToBaseFromArrayListDateData(ArrayList<DateData> dateDataArrayList) {
        StringBuilder stringBuilder = null;
        String data = null;
        for (DateData dateDataFromArrayList : dateDataArrayList) {
            stringBuilder.append(stringToBaseFromDateData(dateDataFromArrayList))
            .append(DataContract.DatabaseEntry.FULL_DATES_SEPARATOR);
        }
        data = stringBuilder.toString();
        return data;
    }

    private String stringToBaseFromHashMapUuidAndDates(HashMap<UUID, ArrayList<DateData>> uuidArrayListHashMap) {
        StringBuilder stringBuilder = new StringBuilder();
        for (HashMap.Entry<UUID, ArrayList<DateData>> entry : uuidArrayListHashMap.entrySet()) {
            stringBuilder.append(entry.getKey())
                    .append(DataContract.DatabaseEntry.MAP_UUID_SEPARATOR)
                    .append(stringToBaseFromArrayListDateData(entry.getValue()))
                    .append(DataContract.DatabaseEntry.MAP_ENTRIES_SEPARATOR);
        }
        return stringBuilder.toString();
    }

    private HashMap<UUID, ArrayList<DateData>> uuidArrayListHashMapFromBaseString(String stringFromBase) {
        HashMap<UUID, ArrayList<DateData>> uuidArrayListHashMap = new HashMap<>();
        String[] keysAndEntriesFormBaseInStrings = stringFromBase.split(DataContract.DatabaseEntry.MAP_ENTRIES_SEPARATOR);
        for (String keyAndValue : keysAndEntriesFormBaseInStrings) {
            String[] keyAndValueSeparated = keyAndValue.split(DataContract.DatabaseEntry.MAP_UUID_SEPARATOR);
            UUID editorUuid = UUID.fromString(keyAndValueSeparated[0]);
            ArrayList<DateData> editorConfirmedDatesArrayList = stringFromBaseToArrayListDateData(keyAndValueSeparated[1]);
            uuidArrayListHashMap.put(editorUuid, editorConfirmedDatesArrayList);
        }
        return uuidArrayListHashMap;
    }

    private String getEventMembersListArrayDataToStringForDatabase(ArrayList<EventMemberListRecord> membersRecords) {
        StringBuilder stringBuilder = new StringBuilder();
        for (EventMemberListRecord record : membersRecords
                ) {
            stringBuilder.append(setEventMemberListRecordToStringForBase(record))
                    .append(DataContract.DatabaseEntry.RECORD_IN_STRING_SEPARATOR);
        }
        return stringBuilder.toString();
    }

    private void setUpdateEventMembersListInDatabase(String eventUuidInString, String membersListInStirng) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(DataContract.DatabaseEntry.EVENT_MEMBERS_LIST, membersListInStirng);
        sqLiteDatabase.update(DataContract.DatabaseEntry.TABLE_NAME_EVENTS_MEMBERS_LIST, contentValues, DataContract.DatabaseEntry.SELECTION_BY_CERTAIN_EVENT_UUID, new String[]{eventUuidInString});
    }

    @Override
    public String getUserNameSurnameInStringByUuid(UUID userUuid) {
        ArrayList<User> allUsersData = getAllUsersArrayListFromBase();
        StringBuilder stringBuilder = new StringBuilder();
        for (User userInfo : allUsersData) {
            if (userInfo.getUserUUID().equals(userUuid)) {
                stringBuilder.append(userInfo.getName())
                        .append(" ")
                        .append(userInfo.getSurname());
                break;
            }
        }
        return stringBuilder.toString();
    }

    public void destoryAllUsersRecords(){
        sqLiteDatabase.delete(DataContract.DatabaseEntry.TABLE_NAME_USERS,null,null);
        sqLiteDatabase.delete(DataContract.DatabaseEntry.TABLE_NAME_LOGINS,null, null);
    }


    //TODO Here must be placed ASyncTask, that take switch/case condition for run method from top
}
