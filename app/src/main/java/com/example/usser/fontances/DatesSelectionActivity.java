package com.example.usser.fontances;
//Wrong class for wrong idea
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.usser.fontances.data.DBWorker;
import com.example.usser.fontances.data.EventStatus;

import sun.bob.mcalendarview.MCalendarView;

public class DatesSelectionActivity extends AppCompatActivity {

    private Event transmittedEvent;
    private MCalendarView calendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dates_selection);
        transmittedEvent = (Event) getIntent().getParcelableExtra(EventsScreen.getInstance().getEventNameToTransmit());
    }

    public void onBtnSelectDatesClick(View view) {
    }

    private void setUnderConsiderationStatusForEvent (){
        transmittedEvent.setEventStatusEnum(EventStatus.UNDER_CONSIDERATION);
    }

    private void clearEventMembersList(){
        EventMembersList eventMembersList = DBWorker.getInstance(getApplicationContext()).getCertainEventMembersList(transmittedEvent.getEventUUID());
        eventMembersList.clearMembersList();
        DBWorker.getInstance(getApplicationContext()).updateEventMembersListInDatabase(eventMembersList);
    }
}
