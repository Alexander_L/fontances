package com.example.usser.fontances;

import java.util.ArrayList;
import java.util.UUID;

public class EventMembersList {

    private UUID eventUUID;
    private ArrayList<EventMemberListRecord> eventMemberDataArrayList;


    public UUID getEventUUID() {
        return eventUUID;
    }

    public void setEventUUID(UUID eventUUID) {
        this.eventUUID = eventUUID;
    }

    public ArrayList<EventMemberListRecord> getEventMemberDataArrayList() {
        return eventMemberDataArrayList;
    }

    public void setEventMemberDataArrayList(ArrayList<EventMemberListRecord> eventMemberDataArrayList) {
        this.eventMemberDataArrayList = eventMemberDataArrayList;
    }

    public EventMemberListRecord getEventMemberListRecordByMemberUuid(UUID memberUUID) {
        EventMemberListRecord eventMemberListRecord = new EventMemberListRecord();
        for (EventMemberListRecord record : eventMemberDataArrayList //TODO Looks not optimal, need to think about it.
                ) {
            if (record.getEventMemberUuid().equals(memberUUID)) {
                eventMemberListRecord = record;
            }
        }
        return eventMemberListRecord;
    }

    public ArrayList<UUID> getEventMembersUuidArrayList() {
        ArrayList<UUID> eventMembersUuidArrayList = new ArrayList<>();
        for (EventMemberListRecord eventMemberRecord : eventMemberDataArrayList
             ) {
            eventMembersUuidArrayList.add(eventMemberRecord.getEventMemberUuid());
        }
        return eventMembersUuidArrayList;
    }

    public boolean isMemberInListByUuid(UUID checkingUuid){
        return getEventMembersUuidArrayList().contains(checkingUuid);
    }

    public void updateMemberDataInList (EventMemberListRecord updatedRecord){
        int position = 0;
        for (int i = 0; i < eventMemberDataArrayList.size(); i++){
            if(eventMemberDataArrayList.get(i).getEventMemberUuid().equals(updatedRecord.getEventMemberUuid())){
                position = i;
            }
        }
        eventMemberDataArrayList.set(position, updatedRecord);
    }

    public void removeRecordFromList(EventMembersList eventMembersList){
        for (EventMemberListRecord memberListRecord:eventMemberDataArrayList
             ) {
            if(memberListRecord.equals(eventMembersList)){
                eventMemberDataArrayList.remove(memberListRecord);
                break;
            }
        }

    }

    public void removeRecordFromList(UUID eventMemberUuid){
        for (EventMemberListRecord memberListRecord:eventMemberDataArrayList
                ) {
            if(memberListRecord.getEventMemberUuid().equals(eventMemberUuid)){
                eventMemberDataArrayList.remove(memberListRecord);
                break;
            }
        }
    }

    public void clearMembersList() {
        eventMemberDataArrayList.clear();
    }



}