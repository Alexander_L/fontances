package com.example.usser.fontances;

import java.util.UUID;

import sun.bob.mcalendarview.vo.DateData;

public class User {
    private String surname;
    private String name;
    private UUID userUUID;
    private DateData birthday;
    private String pathToAvatarPicture;
    private boolean userActive;

    public User(String surname, String name, UUID userUUID, DateData birthday, String pathToAvatarPicture, boolean userActive) {
        this.surname = surname;
        this.name = name;
        this.userUUID = userUUID;
        this.birthday = birthday;
        this.pathToAvatarPicture = pathToAvatarPicture;
        this.userActive = userActive;
    }

    public String getSurname() {
        return surname;
    }

    public String getName() {
        return name;
    }

    public UUID getUserUUID() {
        return userUUID;
    }

    public DateData getBirthday() {
        return birthday;
    }

    public String getPathToAvatarPicture() {
        return pathToAvatarPicture;
    }

    public boolean isUserActive() {
        return userActive;
    }

    public void setUserActiveFalse(){
        userActive = false;
    }
}