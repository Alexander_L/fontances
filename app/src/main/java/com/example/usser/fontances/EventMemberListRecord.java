package com.example.usser.fontances;

import java.util.UUID;

import sun.bob.mcalendarview.vo.DateData;

public class EventMemberListRecord {
    private UUID eventMemberUuid;
    private DateData dateAndTimeToCome;
    private boolean behindTheWheel;
    private boolean plusOne;
    private boolean withKids;


    public UUID getEventMemberUuid() {
        return eventMemberUuid;
    }

    public void setEventMemberUuid(UUID eventMemberUuid) {
        this.eventMemberUuid = eventMemberUuid;
    }

    public boolean isBehindTheWheel() {
        return behindTheWheel;
    }

    public void setBehindTheWheel(boolean behindTheWheel) {
        this.behindTheWheel = behindTheWheel;
    }

    public boolean isPlusOne() {
        return plusOne;
    }

    public void setPlusOne(boolean plusOne) {
        this.plusOne = plusOne;
    }

    public boolean isWithKids() {
        return withKids;
    }

    public void setWithKids(boolean withKids) {
        this.withKids = withKids;
    }

    public DateData getDateAndTimeToCome() {
        return dateAndTimeToCome;
    }

    public void setDateAndTimeToCome(DateData dateAndTimeToCome) {
        this.dateAndTimeToCome = dateAndTimeToCome;
    }
}
