package com.example.usser.fontances.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import sun.bob.mcalendarview.vo.DateData;

public class ConfirmedDatesCrossingSearcher {

    ConfirmedDatesCrossingSearcher(ArrayList<DateData> planningDatesArrayList, HashMap<UUID, ArrayList<DateData>> hashMapDatesToFindCrossing) {
        this.planningDatesArrayList = planningDatesArrayList;
        this.hashMapDatesToFindCrossing = hashMapDatesToFindCrossing;
    }

    private ArrayList<DateData> planningDatesArrayList;
    private HashMap<UUID, ArrayList<DateData>> hashMapDatesToFindCrossing;
    private ArrayList<DateData> comparingDatesArrayList;
    private ArrayList<DateData> crossedDatesArrayList;
    private DateData crossedDate;
    private boolean haveCrossedDates;


    public ArrayList<DateData> getCrossedDatesArrayList() {
        return crossedDatesArrayList;
    }

    public boolean isHaveCrossedDates() {
        return haveCrossedDates;
    }

    private void setHaveCrossedDates(boolean yepWeHaveCrossedDates) {
        haveCrossedDates = yepWeHaveCrossedDates;
    }


    private void checkArrayListForCrosses() {
        crossedDatesArrayList = new ArrayList<DateData>();
        comparingDatesArrayList = new ArrayList<DateData>();
        if (hashMapDatesToFindCrossing.size() > 0) {
            HashMap.Entry<UUID, ArrayList<DateData>> firstEntry = hashMapDatesToFindCrossing.entrySet().iterator().next();
            if (isCrossedDatesInArrayLists(planningDatesArrayList, firstEntry.getValue())) {

                for (Map.Entry<UUID, ArrayList<DateData>> entry : hashMapDatesToFindCrossing.entrySet()) {
                    ArrayList<DateData> dateDataArrayList = entry.getValue();
                    if (!isCrossedDatesInArrayLists(crossedDatesArrayList, dateDataArrayList)) {
                        setHaveCrossedDates(false);
                        return;
                    } else {
                        setHaveCrossedDates(true);
                    }
                }
            }
        }
    }

    private boolean isCrossedDatesInArrayLists(ArrayList<DateData> masterDateDataArrayList, ArrayList<DateData> comparedDateDataArrayList) {
        boolean isCrossed = false;
        for (DateData masterDate : masterDateDataArrayList) {
            if (comparedDateDataArrayList.contains(masterDate)) {
                isCrossed = true;
                comparingDatesArrayList.add(masterDate);
            }
        }
        crossedDatesArrayList = comparingDatesArrayList;
        comparingDatesArrayList = new ArrayList<DateData>();
        return isCrossed;
    }


    private void setCrossedDate(DateData foundedCrossedDate) {
        crossedDate = foundedCrossedDate;
    }

//4 days for 25 lines with code. So sad to be so dumb.


    //region commented method for stock
    /*
        private boolean isCrossedDates(){
            boolean crossedDates = false;
            ArrayList <DateData> datesToCheck = new ArrayList<>();
            datesToCheck.add(planningEventDate);
            for (Map.Entry<UUID, ArrayList<DateData>> entry: hashMapDatesToFindCrossing.entrySet()){
                UUID editorUUID = entry.getKey();
                ArrayList <DateData> dateDataArrayList = entry.getValue();
            }


            return crossedDates;
        }
    */


//endregion
}
