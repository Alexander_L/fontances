package com.example.usser.fontances;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.usser.fontances.data.DBWorker;
import com.example.usser.fontances.util.EventParser;

import java.util.ArrayList;

public class EventStatusCardActivity extends AppCompatActivity {
    private Event transmittedEvent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_status_card);
        transmittedEvent = (Event) getIntent().getParcelableExtra(EventsScreen.getInstance().getEventNameToTransmit());
        TextView eventName = findViewById(R.id.tv_event_status_card_event_name);
        eventName.setText(transmittedEvent.getEventName());
        TextView eventOccasion = findViewById(R.id.tv_event_status_card_event_occasion);
        eventOccasion.setText(transmittedEvent.getEventOccasionEnum().toString());
        TextView eventStatus = findViewById(R.id.tv_event_status_card_event_status);
        eventStatus.setText(transmittedEvent.getEventStatusEnum().toString());
        TextView votedUsers = findViewById(R.id.tv_event_status_card_users_voted_status);
        /*int allUsersWithoutInitiator = DBWorker.getInstance().getAllUsersArrayListFromBase().size() - 1; // -1 event creator;
        int votedUsersWithoutInitiator = transmittedEvent.getHashMapConfirmedDates().size();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Voted ").append(allUsersWithoutInitiator).append(" of ").append(votedUsersWithoutInitiator).append(".");
        votedUsers.setText(stringBuilder.toString());*/

        ArrayList <String> votedUsersArrayList = EventParser.getInstance(getApplicationContext()).userNamesFromHashmap(transmittedEvent.getHashMapConfirmedDates());
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(R.string.voted).append(":\n");
        for (String username : votedUsersArrayList){
            stringBuilder.append(username).append("\n");
        }
        votedUsers.setText(stringBuilder.toString());
    }
}
