package com.example.usser.fontances;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.example.usser.fontances.data.DBWorker;
import com.example.usser.fontances.data.EventStatus;

import java.util.ArrayList;
import java.util.UUID;

import sun.bob.mcalendarview.MCalendarView;
import sun.bob.mcalendarview.vo.DateData;

public class ConfirmationEventScreenActivity extends AppCompatActivity {

    private Event transmittedEvent;
    private MCalendarView calendarView;

    @Override
    public void onCreate(Bundle savedBundleInstance) {
        super.onCreate(savedBundleInstance);
        setContentView(R.layout.confirmation_event_screen);
        transmittedEvent = (Event) getIntent().getParcelableExtra(EventsScreen.getInstance().getEventNameToTransmit());
        TextView tvEventName = findViewById(R.id.tv_Event_Confirmation_event_name);
        TextView tvEventOccasion = findViewById(R.id.tv_Event_Confirmation_occasion);
        TextView tvSuggestedDates = findViewById(R.id.tv_Event_Confirmation_suggested_dates);
        TextView tvEventStatus = findViewById(R.id.tv_Event_Confirmation_status);
        tvEventName.setText(transmittedEvent.getEventName());
        tvEventOccasion.setText(transmittedEvent.getEventOccasionEnum().toString());
        tvSuggestedDates.setText(getPlannedDatesFromArrayList(transmittedEvent.getSuggestedDates()));
        tvEventStatus.setText(transmittedEvent.getEventStatusEnum().toString());
        calendarView = findViewById(R.id.calendarView_event_confirmating);

    }

    public void onBtnSelectSuggestedDatesClick (View view) {
        transmittedEvent.setEventStatusEnum(EventStatus.UNDER_CONSIDERATION);
        ArrayList<DateData> markedDates = new ArrayList<DateData>();
        markedDates = calendarView.getMarkedDates().getAll();
        UUID editorUUID = Login.getInstance().getUserUUID();
        transmittedEvent.setHashMapConfirmedDatesElement(editorUUID, markedDates);
        DBWorker.getInstance(getApplicationContext()).addEventToBase(transmittedEvent);
        Toast.makeText(this, R.string.ok,Toast.LENGTH_SHORT).show();
        finish();

    }

    public void onBtnRejectConfirmationDatesClick (View view) {
        transmittedEvent.setEventStatusEnum(EventStatus.REJECTED_BY_USER);
        DBWorker.getInstance(getApplicationContext()).addEventToBase(transmittedEvent);
        Toast.makeText(this, R.string.reject,Toast.LENGTH_SHORT).show();
        finish();
//TODO Here need logic for rejection event
    }

    private String getPlannedDatesFromArrayList (ArrayList<DateData> plannedDates){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(R.string.proposed_dates);
        for(DateData onePlannedDate : plannedDates){
            stringBuilder.append(onePlannedDate.getDayString())
                    .append("-")
                    .append(onePlannedDate.getMonthString())
                    .append("-")
                    .append(String.valueOf(onePlannedDate.getYear()))
            .append("\n");
        }
        return stringBuilder.toString();
    }

}
