package com.example.usser.fontances.data;

public enum EventOccasion {
    DRINK, EAT, GAME
}