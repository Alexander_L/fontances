package com.example.usser.fontances.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.usser.fontances.User;
import com.example.usser.fontances.data.DataContract.DatabaseEntry;

public class DBHelper extends SQLiteOpenHelper {
    private static DBHelper instance;

    private static final String DATABASE_NAME = DatabaseEntry.DATABASE_NAME;
    private static final int DATABASE_VERSION = Integer.parseInt(DatabaseEntry.DATABASE_VERSION);


    private DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static DBHelper getInstance(Context ctx){
/*        Context ctx = null;
        ctx.getApplicationContext();//I don`t now, how it correct here too.*/
        if(instance == null){
            instance = new DBHelper(ctx.getApplicationContext());
        } //TODO Check here for context!!!
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String SQL_CREATE_EVENTS_TABLE = "CREATE TABLE IF NOT EXISTS " + DatabaseEntry.TABLE_NAME_EVENTS + " ("
                + DatabaseEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DatabaseEntry.EVENT_NAME + " TEXT,"
                + DatabaseEntry.EVENT_UUID + " TEXT,"
                + DatabaseEntry.EVENT_OCCASION + " TEXT,"
                + DatabaseEntry.EVENT_STATUS + " TEXT,"
                + DatabaseEntry.SUGGESTED_DATE + " TEXT,"
                + DatabaseEntry.CONFIRMED_DATES + " TEXT,"
                + DatabaseEntry.ACCEPTED_DATE + " TEXT,"
                + DatabaseEntry.USER_INITIATOR_UUID + " TEXT,"
                + DatabaseEntry.USER_EDITOR_UUID + " TEXT,"
                + DatabaseEntry.MAP_OF_CONFIRMED_DATES + " TEXT);";

        String SQL_CREATE_USERS_TABLE = "CREATE TABLE IF NOT EXISTS " + DatabaseEntry.TABLE_NAME_USERS + " ("
                + DatabaseEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DatabaseEntry.USER_SURNAME + " TEXT,"
                + DatabaseEntry.USER_NAME + " TEXT,"
                + DatabaseEntry.USER_UUID + " TEXT,"
                + DatabaseEntry.USER_BIRTHDAY + " TEXT,"
                + DatabaseEntry.USER_PATH_TO_AVATAR + " TEXT,"
                + DatabaseEntry.USER_ACTIVE + " BOOLEAN);";

        String SQL_CREATE_LOGINS_TABLE = "CREATE TABLE IF NOT EXISTS " + DatabaseEntry.TABLE_NAME_LOGINS + " ("
                + DatabaseEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DatabaseEntry.USER_LOGIN + " TEXT,"
                + DatabaseEntry.USER_PASSWORD + " TEXT,"
                + DatabaseEntry.USER_UUID + " TEXT);";

        String SQL_CREATE_EVENTS_MEMBERS_LISTS_TABLE = "CREATE TABLE IF NOT EXISTS " + DatabaseEntry.TABLE_NAME_EVENTS_MEMBERS_LIST + " ("
                + DatabaseEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DatabaseEntry.EVENT_UUID + " TEXT," //Maybe it can be primary key.
                + DatabaseEntry.EVENT_MEMBERS_LIST + " TEXT);";


        db.execSQL(SQL_CREATE_EVENTS_TABLE);
        db.execSQL(SQL_CREATE_USERS_TABLE);
        db.execSQL(SQL_CREATE_LOGINS_TABLE);
        db.execSQL(SQL_CREATE_EVENTS_MEMBERS_LISTS_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + DatabaseEntry.TABLE_NAME_EVENTS);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseEntry.TABLE_NAME_USERS);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseEntry.TABLE_NAME_LOGINS);
        db.execSQL("DROP TABLE IF EXISTS " + DatabaseEntry.TABLE_NAME_EVENTS_MEMBERS_LIST);

        onCreate(db);

    }




}
