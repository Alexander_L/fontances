package com.example.usser.fontances.util;

import android.content.Context;

import com.example.usser.fontances.Event;
import com.example.usser.fontances.EventMembersList;
import com.example.usser.fontances.data.DBWorker;
import com.example.usser.fontances.data.EventStatus;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

import sun.bob.mcalendarview.vo.DateData;

public class EventCompiler {
    private ArrayList<Event> compiledEventsArrayList;
    private static EventCompiler instance;
    private Context ctx;

    private EventCompiler (Context context){
        ctx = context;
    }

    public ArrayList<Event> getCompiledEventsArrayList() {
        compileEventsArrayList();
        sortEventsArrayListForStatus(compiledEventsArrayList);
        return compiledEventsArrayList;
    }


    public static EventCompiler getInstance(Context incomingContext) {
        if (instance == null) {
            instance = new EventCompiler(incomingContext);
        }
        return instance;
    }

    private void compileEventsArrayList() { //May be not compiling event records from base, but update (not writing new) record in base.
        //This class compile all events records with same event UUID to events Array list by one event - one instance;
        ArrayList<Event> eventsStringsFromBase = DBWorker.getInstance(ctx).getAllEventsRecordsArrayListFromBase(); //TODO Look here - everywhere DBWorker takes getApplicationContext(), but here I must get external context from constructor (and there I give link for getAppContext to). It`s looks wrong.
        compiledEventsArrayList = new ArrayList<>();
        if(eventsStringsFromBase.size() > 0){
            UUID tempEventUUID = eventsStringsFromBase.get(0).getEventUUID();
            Event buildingEvent = eventsStringsFromBase.get(0);
            ArrayList<Event> eventRecordsWithSameUUID = new ArrayList<Event>();
            EventMembersList eventMembersList = new EventMembersList();
            for (Event compilingEventFromArrayList : eventsStringsFromBase) {
                if (tempEventUUID.equals(compilingEventFromArrayList.getEventUUID())) { //TODO Refactor here - too heavy
                    eventRecordsWithSameUUID.add(buildingEvent);

                } else {
                    UUID eventUUID = buildingEvent.getEventUUID(); //TODO Verify here, too heavy and to complicated
                    eventMembersList = DBWorker.getInstance(ctx).getCertainEventMembersList(eventUUID);
                    if (eventRecordsWithSameUUID.contains(buildingEvent.getEventStatusEnum().equals(EventStatus.REJECTED_BY_USER)) && eventMembersList.isMemberInListByUuid(buildingEvent.getEventEditorUUID())) {
                        Event eventMayBeRejected = eventRecordsWithSameUUID.get(eventRecordsWithSameUUID.size() - 1); //Take last record for current event instance.
                        eventMayBeRejected.setEventStatusEnum(EventStatus.REJECTED_BY_USER);
                        compiledEventsArrayList.add(eventMayBeRejected);
                    } else {
                        compiledEventsArrayList.add(eventRecordsWithSameUUID.get(eventRecordsWithSameUUID.size() - 1)); //Take last record for current event instance.
                    }
                    tempEventUUID = compilingEventFromArrayList.getEventUUID();
                    buildingEvent = compilingEventFromArrayList.getEventInstance();
                }
            }
        }
    }

    private void sortEventsArrayListForStatus(ArrayList<Event> eventArrayListToStatusSort) {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(calendar.YEAR);
        int month = calendar.get(calendar.MONTH) + 1;
        int day = calendar.get(calendar.DAY_OF_MONTH);
        DateData currentDateData = new DateData(year, month, day);

        int eventMembersNumber = DBWorker.getInstance(ctx).getAllUsersArrayListFromBase().size() - 1; //-1 user initiator
        for (Event event : eventArrayListToStatusSort
                ) {
            if (event.getEventStatusEnum().equals(EventStatus.UNDER_CONSIDERATION) && event.getHashMapConfirmedDates().size() == eventMembersNumber) {
                ConfirmedDatesCrossingSearcher confirmedDatesCrossingSearcher = new ConfirmedDatesCrossingSearcher(event.getSuggestedDates(), event.getHashMapConfirmedDates());
                if (confirmedDatesCrossingSearcher.isHaveCrossedDates()) {
                    event.setEventStatusEnum(EventStatus.APPROVING);
                    event.setSuggestedDates(confirmedDatesCrossingSearcher.getCrossedDatesArrayList());
                    event.hasClearHashMapConfirmedDates();
                } else {
                    event.setEventStatusEnum(EventStatus.REJECTED_BY_SYSTEM);
                }
            }
            if (event.getEventStatusEnum().equals(EventStatus.APPROVING) && event.getHashMapConfirmedDates().size() == eventMembersNumber) {
                ConfirmedDatesCrossingSearcher confirmedDatesCrossingSearcher = new ConfirmedDatesCrossingSearcher(event.getSuggestedDates(), event.getHashMapConfirmedDates());
                if (confirmedDatesCrossingSearcher.isHaveCrossedDates()) {
                    event.setEventStatusEnum(EventStatus.ACCEPTED);
                } else {
                    event.setEventStatusEnum(EventStatus.REJECTED_BY_SYSTEM);
                }
            }

            if (event.getEventStatusEnum().equals(EventStatus.ACCEPTED)
                    && event.getAcceptedDate().getYear() < currentDateData.getYear()
                    && event.getAcceptedDate().getMonth() < currentDateData.getMonth()
                    && event.getAcceptedDate().getDay() < currentDateData.getDay()
                    && event.getAcceptedDate().getHour() < currentDateData.getHour()
                    && event.getAcceptedDate().getMinute() < currentDateData.getMinute()) {
                event.setEventStatusEnum(EventStatus.SPEND);
            }


        }
    }

}
