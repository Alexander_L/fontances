package com.example.usser.fontances.testpack;

import com.example.usser.fontances.Event;
import com.example.usser.fontances.EventMembersList;
import com.example.usser.fontances.User;
import com.example.usser.fontances.data.EventOccasion;
import com.example.usser.fontances.data.EventStatus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.UUID;

import sun.bob.mcalendarview.vo.DateData;


//This class emulates DBHelper;
public class TestDBData implements com.example.usser.fontances.data.DataBaseWorkInterface {
    private static TestDBData instance;

    @Override
    public boolean checkUserInBase(String login, String password) {
        String correctLogin = "CorrectLogin";
        String correctPassword = "1234";
        return (login.equals(correctLogin) && password.equals(correctPassword)); //Correct here?
    }


    public User getCurrentUserInfo(String login, String password) {
        //Here check from base by login and password
        String surname = "Mad";
        String name = "Max";
        UUID uuid = UUID.randomUUID();
        DateData birthday = new DateData(1988, 02,22);
        String pathToAvatarPicture = "here";
        return new User(surname, name, uuid, birthday, pathToAvatarPicture, true);
    }

    public ArrayList<Event> getEventsArrayList() {
        return TestDBSubstitute.getInstance().getEventArrayList();
    }


    @Override
    public void addEventToBase(Event createdEvent) {
        TestDBSubstitute.getInstance().addEventToArrayList(createdEvent);
    }

    @Override
    public ArrayList<Event> getCreatedEvents() {
        return null;
    }

    @Override
    public void updateEventMembersListInDatabase(EventMembersList updatedEventMemberList) {

    }

    private static User getTestUser() {
        String surname = "MadTest";
        String name = "MaxTest";
        UUID uuid = UUID.randomUUID();
        DateData birthday = new DateData(1988, 02,23);
        String pathToAvatarPicture = "here";
        return new User(surname, name, uuid, birthday, pathToAvatarPicture, true);
    }

    /*public static Event getTestEvent() {
        String eventName = "HardParty";
        UUID eventUUID = UUID.randomUUID();
        Enum<EventOccasion> eventOccasionEnum = EventOccasion.DRINK;
        Enum<EventStatus> eventStatusEnum = EventStatus.CREATE;
        Calendar suggestedDate = Calendar.getInstance();
        Calendar acceptedDate = Calendar.getInstance();
        ArrayList<Calendar> confirmedDates = new ArrayList<>();
        confirmedDates.add(acceptedDate);
        User eventInitiator = getTestUser();
        return new Event(eventName, eventUUID, eventOccasionEnum, eventStatusEnum, suggestedDate, acceptedDate, confirmedDates, eventInitiator);
    }*/

    public ArrayList<User> getUsersList() {
        ArrayList<User> userArrayList= new ArrayList<>();
        userArrayList.add(getTestUser());
        userArrayList.add(getTestUser());
        userArrayList.add(getTestUser());
        return userArrayList;
    }


    public static TestDBData getInstance() {
        if (instance == null) {
            instance = new TestDBData();
        }
        return instance;
    }


    @Override
    public void addLoginInBase(String login, String password, UUID userUUID) {

    }

    @Override
    public ArrayList<UUID> getActiveUsersUuidArrayListFromBase() {
        return null;
    }

    @Override
    public void addEventMembersListToBase(EventMembersList eventMembersList) {

    }

    @Override
    public EventMembersList getCertainEventMembersList(UUID eventUUID) {
        return null;
    }

    @Override
    public ArrayList<EventMembersList> getAllEventsMembersList() {
        return null;
    }

    @Override
    public void addUserInBase(User createdUser) {

    }

    @Override
    public UUID getUserUUIDFromBase(String login, String password) {
        return null;
    }

    @Override
    public User getCurrentUserInfoFromBase(UUID userUUID) {
        return null;
    }

    @Override
    public ArrayList<User> getAllUsersArrayListFromBase() {
        return null;
    }

    @Override
    public ArrayList<Event> getAllEventsRecordsArrayListFromBase() {
        return null;
    }


    public ArrayList<Event> getEventsArrayListFromBase() {
        return null;
    }

    @Override
    public String getUserNameSurnameInStringByUuid(UUID userUuid) {
        return null;
    }
}

