package com.example.usser.fontances;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.usser.fontances.data.DBWorker;
import com.example.usser.fontances.data.EventStatus;
import com.example.usser.fontances.util.EventParser;

import java.util.ArrayList;

import sun.bob.mcalendarview.MCalendarView;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.vo.DateData;

public class FinalEventDateChoiseScreenActivity extends AppCompatActivity {

    private Event transmittedEvent;
    private MCalendarView calendarView;
    private ArrayList<DateData> markedDates;
    private boolean dateMarked;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.final_event_date_choise_screen);
        transmittedEvent = (Event) getIntent().getParcelableExtra(EventsScreen.getInstance().getEventNameToTransmit());
        TextView eventName = findViewById(R.id.tv_event_final_choose_event_name);
        eventName.setText(transmittedEvent.getEventName());
        TextView eventOccasion = findViewById(R.id.tv_event_final_choose_occasion);
        eventOccasion.setText(transmittedEvent.getEventOccasionEnum().toString());
        TextView eventStatus = findViewById(R.id.tv_event_final_choose_status);
        eventStatus.setText(transmittedEvent.getEventStatusEnum().toString());
        TextView eventUsersConfirmationStatus = findViewById(R.id.tv_event_final_choose_event_users_confirmation_status);
        /*int allUsersWithoutInitiator = DBWorker.getInstance().getAllUsersArrayListFromBase().size() - 1; // -1 event creator;
        int votedUsersWithoutInitiator = transmittedEvent.getHashMapConfirmedDates().size();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Voted ").append(allUsersWithoutInitiator).append(" of ").append(votedUsersWithoutInitiator).append(".");
        eventUsersConfirmationStatus.setText(stringBuilder.toString());*/
        ArrayList<String> votedUsersArrayList = EventParser.getInstance(getApplicationContext()).userNamesFromHashmap(transmittedEvent.getHashMapConfirmedDates());
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(R.string.confirmed).append(":\n");
        for (String username : votedUsersArrayList) {
            stringBuilder.append(username).append("\n");
        }
        eventUsersConfirmationStatus.setText(stringBuilder.toString());

        calendarView = ((MCalendarView) findViewById(R.id.calendarView_event_final_choising));


        calendarView.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(View view, DateData date) {
                dateMarked = false;
                int m = 0;
                for (DateData i : markedDates
                        ) {
                    if (i.equals(date)) {
                        dateMarked = true;
                        m = markedDates.indexOf(i);
                    }
                }
                if (!dateMarked) {
                    calendarView.markDate(date);
                    markedDates.add(date);
                } else if (dateMarked) {
                    markedDates.remove(m);
                    calendarView.unMarkDate(date);
                }
                Toast.makeText(FinalEventDateChoiseScreenActivity.this, String.format("%d-%d", date.getDay(), date.getMonth()), Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void onBtnFianlDateConfirmClick(View view) {
        //if (hasConfirmedDates() && oneCreatedDay()){
        if (hasOneConfirmedDate()) {
            actionByUserRole();
            DBWorker.getInstance(getApplicationContext()).addEventToBase(transmittedEvent);
        } else {
            Toast.makeText(this, R.string.select_one_or_more_event_dates, Toast.LENGTH_SHORT).show();
        }
    }

/*    private boolean oneCreatedDay(){
        return markedDates.size() != 0 && !(markedDates.size() > 1); // why not == 1 ?
    }

    private boolean hasConfirmedDates (){
        return !markedDates.isEmpty();
    }*/

    private void actionByUserRole() {
        if (Login.getInstance().currentUser.getUserUUID().equals(transmittedEvent.getEventInitiatorUUID())) {
            transmittedEvent.setAcceptedDate(markedDates.get(0));
        } else {

            transmittedEvent.setHashMapConfirmedDatesElement(Login.getInstance().getCurrentUser().getUserUUID(), markedDates);
            transmittedEvent.setEventStatusEnum(EventStatus.APPROVING);
        }
    }


    private boolean hasOneConfirmedDate() {
        return markedDates.size() == 1;
    }

}